let port = process.env.PORT || 3005;
let database = process.env.DATABASE || 'consectus_rpa';
let database_user = process.env.DATABASE_USER || 'consectus_rpa';
let database_password = process.env.DATABASE_PASSWORD || 'itiss@f3';
let database_host = process.env.DATABASE_HOST || 'localhost';
let database_port = process.env.DATABASE_PORT || "3306";

module.exports = {
    'secret': 'xyz-abc-def',
    'app_port': port,
    "defaultnamespace": "cf30291d-76d6-4ec1-8569-d70f5148bad4",
    'expiresIn': '7200000',
    'codeLifetimeSeconds': 300,
    "database": {
        "host": database_host,
        "user": database_user,
        "password": database_password,
        "database": database,
        "port": database_port,
        "create": false
    },
    "tables": {
        clients: "oauth2_clients",
        tokens: "oauth2_access_tokens",
        codes: "oauth2_authorization_codes",
        users: "oauth2_users",
        permission: "oauth2_roles",
        url: "oauth2_urls_permission"
    },
    "clients": [
        {
            client_id: "consectus-rpa", name: "Consectus RPA", client_sec: "#xyz-@abc-$e3a-$#be-&@4e", redirect_uri: null
        },
        {
            client_id: "consectus-api", name: "Consectus API", client_sec: "#xyz-@abc-$e3a-$#be-&@4e", redirect_uri: null
        }],
    "users": [
        {
            admin_username: 'admin', admin_email: 'superadmin@admin.com', admin_password: 'password', admin_firstname: 'super'
            , admin_lastname: 'admin', admin_client: 'consectus-api', permission: '/*'
        }],
    "bcrypt": {
    },
}
