const seed = require("./dbseeder")
const config = require('../oauth-config/config');

// db.initConnection(config)
const debug = require("debug")("consectus.migration.oauth2");

const createDbSql = 'create database if not exists ' + config.database.database;

async function createDB(executeQuery) {
    return await executeQuery(createDbSql);
}



async function initialize(executeQuery) {
    if (config.database.create) {
        try {
            await createDB(executeQuery);
        } catch (error) {
            debug("Create database failed .... please check permissions for creating database");
        }
    }

    //check to ensure that all the database tables are correctly created
    //TYPE nodejs or script 
    const createClientTokenSql = `CREATE TABLE IF NOT EXISTS ${config.tables.clients} (
        id int(11) NOT NULL AUTO_INCREMENT,
        client_id varchar(100) NOT NULL,
        name varchar(45) DEFAULT NULL,
        client_secret varchar(100) DEFAULT NULL ,
        redirect_uri varchar(255) DEFAULT '/oauth/code',
        PRIMARY KEY (id),
        UNIQUE KEY oauth2_client_client_id_unique (client_id),
        KEY oauth2_client_client_id_idx (client_id)        
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;

    const createAccessTokenSql = `CREATE TABLE IF NOT EXISTS ${config.tables.tokens} (
        id int(11) NOT NULL AUTO_INCREMENT,
        user_id int(11) DEFAULT NULL,
        token varchar(100) NOT NULL,
        code varchar(100) DEFAULT NULL,
        client_id varchar(100) NOT NULL,
        expires datetime NOT NULL,
        created_date datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        UNIQUE KEY oauth2_token_token_unique (token),
        KEY oauth2_token_token_idx (token),
        KEY oauth2_token_code_idx (code),
        KEY oauth2_token_client_id_idx (client_id),
        CONSTRAINT oauth2_token_client_id_fk FOREIGN KEY (client_id) REFERENCES ${config.tables.clients} (client_id) ON DELETE CASCADE,
        CONSTRAINT oauth2_token_user_id_fk FOREIGN KEY (user_id) REFERENCES ${config.tables.users} (id) ON DELETE CASCADE
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    `;
    const createUserSql = `CREATE TABLE IF NOT EXISTS ${config.tables.users} (     
        id int(11) NOT NULL AUTO_INCREMENT,
        username varchar(40) NOT NULL,
        email_id varchar(60) NOT NULL,
        firstname varchar(15) NOT NULL,
        lastname varchar(15) NOT NULL,
        password varchar(100) NOT NULL,  
        client_id varchar(100) NOT NULL,
        status int(11) DEFAULT 0,
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        UNIQUE KEY oauth2_users_email_id_unique (email_id),
        KEY oauth2_users_email_id_password_client_id_idx (email_id,password,client_id),
        KEY oauth2_users_status_idx (status),
        CONSTRAINT oauth2_users_client_id_fk FOREIGN KEY (client_id) REFERENCES ${config.tables.clients} (client_id) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
`;

    const createCodeSql = `CREATE TABLE IF NOT EXISTS ${config.tables.codes} (
        id int(11) NOT NULL AUTO_INCREMENT,
        code varchar(100) DEFAULT NULL,
        code_challenge varchar(500) NOT NULL,
        code_challenge_method varchar(10) NOT NULL,
        client_id varchar(100) NOT NULL,
        state varchar(100) DEFAULT NULL,
        used int(11) DEFAULT '0',
        created_date datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        UNIQUE KEY oauth2_code_code_unique (code),
        KEY oauth2_code_code_challenge_idx (code_challenge,code),
        KEY oauth2_code_client_id_idx (client_id),
        CONSTRAINT oauth2_code_client_id_fk FOREIGN KEY (client_id) REFERENCES ${config.tables.clients} (client_id) ON DELETE CASCADE
      ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
    `;

    const createActiveSql = `CREATE TABLE IF NOT EXISTS ${config.tables.url} (
        id int(11) NOT NULL AUTO_INCREMENT,
        url varchar(500) NOT NULL,   
        PRIMARY KEY (id),
        KEY oauth2_code_url_idx (url)  
      ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
    `;
    const createPermissionSql = `CREATE TABLE IF NOT EXISTS ${config.tables.permission} (
        id int(11) NOT NULL AUTO_INCREMENT,
        user_id int(11) NOT NULL,
        url_id int(11) NOT NULL,   
        PRIMARY KEY (id),
        KEY oauth2_code_user_id_idx (user_id),
        KEY oauth2_code_url_id_idx (url_id),
        CONSTRAINT oauth2_code_user_id_fk FOREIGN KEY (user_id) REFERENCES ${config.tables.users} (id) ON DELETE CASCADE,
        CONSTRAINT oauth2_code_url_id_fk FOREIGN KEY (url_id) REFERENCES ${config.tables.url} (id) ON DELETE CASCADE
      ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
    `;



    try {
        await executeQuery(createClientTokenSql);

        await executeQuery(createUserSql);
        await executeQuery(createAccessTokenSql);

        await executeQuery(createCodeSql);
        await executeQuery(createActiveSql);

        await executeQuery(createPermissionSql);

        for (let index = 0; index < config.clients.length; index++) {
            //create clients if not exist
            try {

                 await seed.ensureClient(config.clients[index],executeQuery).then((result) => {
                    debug("Database clients verified");
                 });

            } catch (error) {
                throw error
            }
        }
        for (let index = 0; index < config.users.length; index++) {
            //create clients if not exist
            try {

                await seed.ensureUser(config.users[index],executeQuery).then((result) => {

                    debug("Database users verified");
                 });
                 await seed.Createpermission(config.users[index],executeQuery).then((result) => {

                //     debug("Database permissions verified");
                 });
            } catch (error) {
                throw error;
            }
        }




    } catch (error) {
        throw error;
    }

}
module.exports.initialize = initialize;
