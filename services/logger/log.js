var winston = require('winston');
const fs = require("fs");
const path = require("path");
const config = require("../../config/config");
const logFolder = path.join(__dirname, '/../../logs');
const debug = require("debug")("consectus.logger");
if (!fs.existsSync(logFolder)) {
    debug("creating logs folder");
    fs.mkdirSync(logFolder);
}
var logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(info => {
            return `[${info.timestamp}] : [${info.level}] : ${info.message}`;
        })
    ),
    transports: [
        // new (winston.transports.Console)({ json: false, timestamp: true }),
        new winston.transports.File({ filename: __dirname + '/../../logs/debug.log' })
    ],
    exceptionHandlers: [
        // new (winston.transports.Console)({ json: false, timestamp: true }),
        new winston.transports.File({ filename: __dirname + '/../../logs/exceptions.log' })
    ],
    exitOnError: false // do not exit on handled exceptions
});

module.exports = logger;