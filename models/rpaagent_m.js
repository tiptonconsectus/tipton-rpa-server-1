const config = require("../config/config");
const executeQuery = require("../services/dbhelper/sqlhelper").executeQuery;
const mysql = require("mysql");
class RPAModel {
    constructor() {
        //   this.executeQuery=require("../services/dbhelper/sqlhelper").executeQuery;

    }
    find(data = null) {
        let query = "SELECT *  FROM ?? WHERE isdelete=0"
        let inserts = [config.tables.rpaAgent]
        if (data !== null) {
            //   let index = 0;
            for (let key in data) {
                // if (index == 0) {
                //     query += " WHERE ??=?"
                //     inserts.push(key)
                //     inserts.push(data[key])
                // } else {
                query += " AND ??=?"
                inserts.push(key)
                inserts.push(data[key])
                // }
                // index++
            }
        }
        // query += " ORDER BY queue_size ASC "
        query = mysql.format(query, inserts)
        return new Promise((resolve, reject) => {
            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {

                reject(error)
            })
        })

    }
    // findAgent(data) {
    //     let query =  "SELECT *  FROM ?? WHERE agent_id=? and isdelete=0" 
    //     let inserts = [config.tables.rpaAgent,data]

    //     query = mysql.format(query, inserts)
    //      console.log(query)
    //     return new Promise((resolve, reject) => {

    //         executeQuery(query).then((result) => {
    //             resolve(result);

    //         }).catch((error) => {
    //             console.log(error)

    //             reject(error)
    //         })
    //     })

    // }
    CreateAgent(data) {


        let query = "INSERT INTO ?? (??,??,??,??,??,??) VALUES(?,?,?,?,?,?)"
        let inserts = [config.tables.rpaAgent, 'agent_name', 'agent_email', 'host', 'port', 'settimeout', 'score', data.agentname, data.adminemail, data.host, data.port, data.settimeout, data.score];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
               // console.log(error)
                reject(error);
            })
        })

    }
    CreateDynamic(id, name, value) {
        let query = "INSERT INTO ?? (??,??,??)"
        for (let i = 0; i < name.length; i++) {
            if (i === 0) {
                query += "  VALUES('" + name[i] + "','" + value[i] + "'," + id + ")"
            } else {
                query += "  ,('" + name[i] + "','" + value[i] + "'," + id + ") "
            }
        }
        let inserts = [config.tables.setting, 'name', 'value', 'agent_id']
        query = mysql.format(query, inserts)
        //   console.log(query)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
              //  console.log(error)
                reject(error)
            })
        })

    }
    updateAgent(data, status = false) {

        let query;
        let inserts;
        if (status === true) {
            query = "UPDATE ?? SET ??=? "
            query += "  WHERE id=" + data.update_id + "";
            inserts = [config.tables.rpaAgent, "status", data.status];
        }
        else {
            query = "UPDATE ?? SET ??=? ,??=? ,??=?,??=?,??=? ,??=? "
            query += "  WHERE id=" + data.update_id + "";
            inserts = [config.tables.rpaAgent, "agent_name", data.agentname, "agent_email", data.adminemail, "host", data.host, "port", data.port, "settimeout", data.timeout, "score", data.score];
        }

        query = mysql.format(query, inserts)
        // console.log(query)
        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            });
        });

    }

    deleteDynamic(data) {
        let query = "DELETE FROM ?? WHERE ??=? ";
        let inserts = [config.tables.setting, "agent_id", data];
        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            });
        });
    }
    deleteAgent(data) {
        let num = /^[0-9]{1,11}$/

        let query = "UPDATE ?? SET isdelete=1 WHERE ??=?";
        let inserts = [config.tables.rpaAgent, "id", data];
        query = mysql.format(query, inserts)

        return new Promise((resolve, reject) => {
            if (num.test(data)) {
                executeQuery(query).then((result) => {
                    resolve(result);

                }).catch((error) => {
                    reject(error)
                });
            } else {
                reject("something went wrong")
            }
        });

    }
    // Find_Agent_ByPathname(data){

    //     //   
    // SELECT rpa_agents.agent_name, action_paths.name, sum(if(action_path_queues.status=2,1,0)) AS `count`,
    // IFNULL(avg(case when action_path_queues.status=2 and action_path_queues.path_id = action_paths.id then action_path_queues.execution_delay  end),0) AS delay
    // FROM (rpa_agents, action_paths) LEFT JOIN action_path_queues ON rpa_agents.id = action_path_queues.agent_id AND action_paths.id = action_path_queues.path_id
    // GROUP BY rpa_agents.agent_name, action_paths.name
    // ORDER BY rpa_agents.agent_name, action_paths.name
    //     let query = "SELECT agent.* ,COUNT(queue.id) queue_count,path.name FROM ?? agent ";
    //        query +=  " LEFT JOIN ?? queue ON queue.agent_id=agent.id "
    //        query +=  " LEFT JOIN ?? path on path.id=queue.path_id  "
    //        query +=  "  WHERE path.??=? and agent.isdelete=0 GROUP BY(path.name) ORDER BY queue_count "
    //     let inserts = [config.tables.rpaAgent,config.tables.queues,config.tables.paths, 'name',data];
    //     query = mysql.format(query, inserts)
    //     console.log(query)
    //     return new Promise((resolve, reject) => {

    //         executeQuery(query).then((result) => {
    //             resolve(result);

    //         }).catch((error) => {
    //             reject(error)
    //         });
    //     });
    // }
    Find_Best_ByAgent(data = null, status = null) {
        let query = "SELECT agent.* ,COUNT(queue.agent_id) queue_count FROM ?? agent ";
        query += " LEFT JOIN "
        query += " (SELECT q.agent_id FROM ?? q INNER JOIN ?? p on q.path_id=p.id WHERE q.status BETWEEN 0 and 1 and q.isdelete = 0 and p.isdelete = 0 and p.order_id <> 0"
        query += (data !== null) ? " AND p.name=?) " : " )"
        query += " queue ON queue.agent_id=agent.id WHERE agent.??=? and agent.isdelete=0 GROUP BY(agent.id) "
        query += " ORDER BY queue_count "
        let inserts = (data !== null) ? [config.tables.rpaAgent, config.tables.queues, config.tables.paths, data, 'status', (status === null) ? 1 : status] : [config.tables.rpaAgent, config.tables.queues, config.tables.paths, 'status', (status === null) ? 1 : status];
        query = mysql.format(query, inserts)
        // console.log(query)
        return new Promise((resolve, reject) => {

            executeQuery(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                reject(error)
            });
        });
    }
}
module.exports = RPAModel;