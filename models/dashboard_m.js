const config = require('../config/config');
const executeQuery = require("../services/dbhelper/sqlhelper").executeQuery;
const mysql = require("mysql");
class Dashboard_m {
    constructor() {

    }
    getAgentPathQueues(status = null) {
        return new Promise((resolve, reject) => {
            let where = "";
            switch (status) {
                case 'pending':
                    where = `
                        WHERE q.status in (0,1)
                    `
                    break;
                case 'completed':
                    where = `
                        WHERE q.status = 2
                    `
                    break;
                case 'all':
                    break;
                default:
                    break;
            }
            if (status !== null) {
            }
            let query = `
SELECT q.id q_id, q.params q_params, q.status q_status, q.result q_result, q.callback_url q_callback_url,
q.created_on q_created_on, q.execution_delay q_execution_delay, q.isdelete q_isdelete,
a.agent_name, a.agent_email, a.host a_host, a.port a_port, a.status a_status, 
p.name p_name, p.type p_type, p.queued p_queued, p.order_id p_order_id
FROM ?? q
        INNER JOIN ?? a on a.id = q.agent_id
        INNER JOIN ?? p on p.id = q.path_id
        ${where}
        ORDER BY a.id, p.id, q.id
        `
            let inserts = [config.tables.queues, config.tables.rpaAgent, config.tables.paths];
            query = mysql.format(query, inserts)
            // console.log(query);
            executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });

    }
    Average(status = null) {
        return new Promise((resolve, reject) => {

            let query = "SELECT p.name,sum(q.execution_delay),IFNULL(avg(case when q.status=2 and q.path_id = p.id then q.execution_delay  end),0) AS AvgScore "
            query += " FROM ?? q LEFT JOIN ?? p on q.path_id=p.id WHERE q.status=2 and q.isdelete = 0 and p.isdelete = 0 GROUP BY p.name"

            let inserts = [config.tables.queues, config.tables.paths];
            query = mysql.format(query, inserts)
            //console.log(query)
            // debug(query);
            executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });

    }
    PathQueues() {
        return new Promise((resolve, reject) => {

            // let query = " SET SESSION sql_mode=`STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION`; "
           // let query =" set session sql_mode=`STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION`; "
           let query = " SELECT a.agent_name,a.status, p.name,a.host,a.port, sum(if(q.status=2,1,0)) AS `completed_count`, "

            query += " sum(if(q.status=-1,1,0)) AS `failed_count`, sum(case when q.status between 0 and 1 then 1 else 0 end) AS `pending_count`, "
            query += " IFNULL(sum(q.status  between -1 and 2),0) AS `total_count`, "
            query += " IFNULL(avg(case when q.status=2 and q.path_id = p.id then q.execution_delay  end),0) AS delay "
            query += " FROM (?? a, ?? p) LEFT JOIN ?? q ON a.id = q.agent_id AND p.id = q.path_id WHERE a.isdelete=0 AND p.isdelete=0 "
            query += " GROUP BY a.agent_name, p.name "
            query += " ORDER BY a.agent_name, p.name"
            // let query = "SELECT p.name,execution_delay,(SELECT AVG(execution_delay) FROM ?? WHERE path_id = p.id and status=2 and agent_id is not null) AS AvgScore "
            //     query +=" FROM ?? q INNER JOIN ?? p on q.path_id=p.id WHERE q.status=2 and q.isdelete = 0 and p.isdelete = 0 " 

            let inserts = [config.tables.rpaAgent, config.tables.paths, config.tables.queues];
            query = mysql.format(query, inserts)
            // console.log(query)
            // debug(query);
            executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
   async getComplete(dataBody) {
      
            let found = false;
            let index = 0;
            let sql = " SELECT q.*,a.agent_name,a.agent_email,a.host,a.port,a.settimeout,a.status rpastat ,a.isdelete from ?? q inner join ?? a on a.id =q.agent_id "
            let total = " SELECT count(*) count from ?? q inner join ?? a on a.id =q.agent_id "
            if (dataBody.tableobj && dataBody.tableobj.where) {
                let whereParams = dataBody.tableobj.where;
                
                for (let keys in whereParams) {
                    if (keys === 'status') whereParams['q.status'] = whereParams[keys], keys = 'q.status';                  
                    if (found) {
                        sql += ` and ${keys} =  '${whereParams[keys]}' `;
                        total += ` and ${keys} =  '${whereParams[keys]}' `;
                    } else {
                        sql += ` WHERE ${keys} =  '${whereParams[keys]}' `;
                        total += ` WHERE ${keys} =  '${whereParams[keys]}' `;
                        found = true;
                    }
                }   
            }
            if (dataBody.tableobj && dataBody.tableobj.searchDetail) {
                let dataSearch = dataBody.tableobj.searchDetail;
                let SearchLength = Object.keys(dataSearch).length;
                for (let key in dataSearch) {
                     if(key==='status') dataSearch['q.status']= dataSearch[key], key = 'q.status' ;
    
                  
                    if (found) {
                        if (index === 0) sql += ` and ( ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` and ( ${key} like ${"'%" + dataSearch[key] + "%'"} `, index = index + 1;
                        else sql += ` or ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` or ${key} like ${"'%" + dataSearch[key] + "%'"} `, index = index + 1;
                    } else {
                        if (index === 0) sql += ` WHERE ${key} like ${"'%" + dataSearch[key] + "%'"} `, total += ` WHERE ${key} like ${"'%" + dataSearch[key] + "%'"} `, found = true, index = index + 1;
                    }
                    if (SearchLength === index) sql += ` ) `,total += ` ) `
    
                }
            }
            if (dataBody.tableobj && dataBody.tableobj.sortDetail) sql += ` ORDER BY ${dataBody.tableobj.sortDetail.field}  ${dataBody.tableobj.sortDetail.type} `
    
            if (dataBody.tableobj && dataBody.tableobj.pageDetail) sql += ` LIMIT ${dataBody.tableobj.pageDetail.pageSize} OFFSET ${dataBody.tableobj.pageDetail.pageSize * dataBody.tableobj.pageDetail.page} `

            let inserts = [config.tables.queues, config.tables.rpaAgent];
            sql = mysql.format(sql, inserts);
            total = mysql.format(total, inserts);

            let totalcount = await executeQuery(total);

            let results = await executeQuery(sql)
            return { data: results, totalcount: totalcount };
      
    }
}
module.exports = Dashboard_m;