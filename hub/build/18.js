webpackJsonp([18],{

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewAccountViewPageModule", function() { return NewAccountViewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__new_account_view__ = __webpack_require__(616);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NewAccountViewPageModule = (function () {
    function NewAccountViewPageModule() {
    }
    return NewAccountViewPageModule;
}());
NewAccountViewPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__new_account_view__["a" /* NewAccountViewPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__new_account_view__["a" /* NewAccountViewPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], NewAccountViewPageModule);

//# sourceMappingURL=new-account-view.module.js.map

/***/ }),

/***/ 616:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewAccountViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_new_account_new_account__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NewAccountViewPage = (function () {
    function NewAccountViewPage(navCtrl, navParams, languageProvider, oauthProvider, loadauditProvider, toastCtrl, alertCtrl, settingsProvider, viewCtrl, newAccountProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.loadauditProvider = loadauditProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.newAccountProvider = newAccountProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.resultdata = null;
        this.languageString = null;
        this.newAccountData = null;
        this.divideParamsData = [];
        this.currenTab = "photoProof";
        this.isPhotoReject = false;
        this.isVideoReject = false;
        this.isAddessReject = false;
        this.videoRejectReason = "selected";
        this.mailerButton = "Send Mailer";
        this.newAccountActivityOptions = {
            data: [],
            filteredData: [],
            columns: {
                "create_date": { title: "Date", format: 'function', fn: this.formatDateTime.bind(this), col: 5 },
                "trail_details": { title: "Trail Details", search: true },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: false,
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            add: {
                addActionTitle: "",
                addAction: null,
            },
            rowActions: [],
            columnClass: "table-column",
            // tapRow: this.showUser.bind(this),
            resetFilter: null,
            uploadOptions: {},
        };
        var self = this;
        self.divideParamsData = this.navParams.get("divideParamsId");
        console.log(self.divideParamsData, "divideParamsData");
        this.newAccountData = this.navParams.get("newAccountData");
        console.log(this.newAccountData, "thisnewAccountData");
        // this.newAccountData.create_date = this.formatDate(this.newAccountData.create_date);
        if (this.newAccountData == undefined || this.newAccountData == null) {
            this.newAccountData = {
                account_types_id: 1,
                accountholdername: "",
                accountinsterest: null,
                accountno: "",
                accountsortcode: "",
                accountstatus: "",
                accountype: "",
                city: "",
                create_date: "",
                customerstatus: "",
                dob: "",
                emailid: "",
                firstname: "",
                gender: "",
                houseno: "",
                lastname: "",
                memodate: "",
                memoname: "",
                memoplace: "",
                middlename: "M",
                modified_date: null,
                mothermaidenname: "",
                nationality: "",
                naccountid: 1,
                password: "",
                postalcode: "",
                state_status: "",
                street: "",
                taxresident: 1,
                title: "",
                phone: "",
                productcode: "",
                photoidproof: ""
            };
        }
        else {
            this.newAccountData.jhFirstname = "Mr K";
            this.newAccountData.jhLastname = "Lion";
            this.newAccountData.create_date = this.formatDateTime(this.newAccountData.create_date);
        }
        this.naccountid = this.newAccountData.naccountid;
        this.state_status = this.newAccountData.state_status;
        console.log(this.naccountid, "thisnaccountid");
    }
    NewAccountViewPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("newaccount")) {
            return true;
        }
        else {
            return false;
        }
    };
    NewAccountViewPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        var message = "Viewed " + this.newAccountData.accountholdername + "in New Account Page";
        console.log(this.oauthProvider.user[0].users_id, "user_id_value");
        var auditUser = {
            // "users_id": this.oauthProvider.user[0].users_id,
            "users_id": this.naccountid,
            "customer_devices_id": 0,
            "usertype": "newaccount",
            "uuid": "",
            "appname": "hub",
            "event_type": "add",
            "screen": "new-account-view",
            "trail_details": message
        };
        this.newAccountProvider.getnewaccountaudits(auditUser)
            .then(function (result) {
            console.log(result, "result");
            if (result.message !== undefined) {
                self.newAccountActivityOptions.data = result.message;
                self.newAccountActivityOptions = Object.assign({}, self.newAccountActivityOptions);
            }
        });
        if (this.newAccountData.state_status == "Existing Mailer Blocked" || this.newAccountData.state_status == "New Existing Mailer Blocked") {
            this.mailerButton = "Approve Blocked";
        }
    };
    NewAccountViewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewAccountViewPage');
    };
    NewAccountViewPage.prototype.formatDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    NewAccountViewPage.prototype.formatDateTime = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            return (newdate);
        }
    };
    NewAccountViewPage.prototype.approveVideo = function () {
        console.log("Approve video");
    };
    NewAccountViewPage.prototype.rejectVideo = function () {
        console.log("Reject video");
        this.isVideoReject = true;
    };
    NewAccountViewPage.prototype.approvePhoto = function () {
        console.log("Approve photo");
    };
    NewAccountViewPage.prototype.rejectPhoto = function () {
        console.log("Reject photo");
        this.isPhotoReject = true;
    };
    NewAccountViewPage.prototype.approveAddress = function () {
        console.log("approve address");
    };
    NewAccountViewPage.prototype.rejectAddress = function () {
        console.log("reject address");
        this.isAddessReject = true;
    };
    NewAccountViewPage.prototype.pending = function () {
        console.log("pending");
    };
    NewAccountViewPage.prototype.approved = function () {
        console.log("rejected");
    };
    NewAccountViewPage.prototype.rejected = function () {
    };
    NewAccountViewPage.prototype.onRejectStatus = function () {
        console.log("select data", this.videoRejectReason);
    };
    NewAccountViewPage.prototype.segmentChanged = function (event) {
        console.log("event", this.currenTab);
    };
    NewAccountViewPage.prototype.approveblocked = function (state_status) {
        var _this = this;
        console.log("approved123", this.naccountid, state_status);
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm"),
            message: self.settingsProvider.getLabel("Are you sure want to approve this customer?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("Ok"),
                    handler: function (data) {
                        console.log(_this.naccountid, "thisnaccountid");
                        self.newAccountProvider.restartregistrationbyaccountsidv2(_this.naccountid, _this.state_status).then(function (result) {
                            var toast = self.toastCtrl.create({
                                message: self.settingsProvider.getLabel(result.message),
                                duration: 3000,
                                position: 'right',
                                cssClass: 'success',
                            });
                            toast.present();
                            if (self.divideParamsData !== undefined) {
                                if (self.divideParamsData.length > 0) {
                                    self.navCtrl.setRoot("NewAccountPage");
                                }
                            }
                            else {
                                self.navCtrl.pop();
                            }
                        });
                    }
                }
            ],
            enableBackdropDismiss: true
        });
        alert.present();
    };
    NewAccountViewPage.prototype.unblockedMailer = function (state_status) {
        var _this = this;
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm"),
            message: self.settingsProvider.getLabel("Are you sure want to approve this customer?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("Ok"),
                    handler: function () {
                        var mailerBlock;
                        if (_this.newAccountData.state_status == "Existing Mailer Blocked") {
                            mailerBlock = {
                                new_accounts_id: _this.naccountid,
                                state_status: "Existing Mailer"
                            };
                            _this.mailerButton = "Approve Blocked";
                        }
                        else if (_this.newAccountData.state_status == "New Existing Mailer Blocked") {
                            mailerBlock = {
                                new_accounts_id: _this.naccountid,
                                state_status: "New Existing Mailer"
                            };
                            _this.mailerButton = "Approve Blocked";
                        }
                        else if (_this.newAccountData.state_status == "New Existing Mailer") {
                            mailerBlock = {
                                new_accounts_id: _this.naccountid,
                                state_status: "New Existing Mailer Send"
                            };
                        }
                        else {
                            mailerBlock = {
                                new_accounts_id: _this.naccountid,
                                state_status: "Existing Mailer Send"
                            };
                        }
                        self.newAccountProvider.blockexistingcustomer(mailerBlock).then(function (result) {
                            var toast = self.toastCtrl.create({
                                message: self.settingsProvider.getLabel(result.message),
                                duration: 3000,
                                position: 'right',
                                cssClass: 'success',
                            });
                            toast.present();
                            if (self.divideParamsData !== undefined) {
                                if (self.divideParamsData.length > 0) {
                                    self.navCtrl.setRoot("NewAccountPage");
                                }
                            }
                            else {
                                self.navCtrl.pop();
                            }
                        });
                    }
                }
            ],
            enableBackdropDismiss: true
        });
        alert.present();
    };
    NewAccountViewPage.prototype.ConfirmAlert = function () {
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm"),
            message: self.settingsProvider.getLabel("Are you sure want to approve this customer?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("Ok"),
                    handler: function (data) {
                        self.navCtrl.pop();
                    }
                }
            ],
            enableBackdropDismiss: true
        });
        alert.present();
    };
    NewAccountViewPage.prototype.DownloadPdf = function (newAccountData) {
        var binary = atob(newAccountData.generated_letter.replace(/\s/g, ''));
        var len = binary.length;
        var buffer = new ArrayBuffer(len);
        var view = new Uint8Array(buffer);
        var j;
        for (j = 0; j < len; j++) {
            view[j] = binary.charCodeAt(j);
        }
        // create the blob object with content-type "application/pdf"          
        var url = URL.createObjectURL(new Blob([view], { type: "application/pdf" }));
        window.open(url);
    };
    return NewAccountViewPage;
}());
NewAccountViewPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-new-account-view',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\new-account-view\new-account-view.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel("Customer Details")}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n\n\n\n\n    <ion-row align-items-start>\n\n      <ion-col col-6>\n\n        <ion-card padding>\n\n          <ion-row>\n\n            <ion-col col-6>\n\n              <ion-row\n\n                *ngIf="newAccountData.title && newAccountData.firstname && newAccountData.lastname != null || newAccountData.title && newAccountData.firstname && newAccountData.lastname != null || newAccountData.title && newAccountData.firstname && newAccountData.lastname != \'\' || newAccountData.title && newAccountData.firstname && newAccountData.lastname != undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel("Full Name")}}</ion-label>\n\n                  <ion-input [disabled]="true"\n\n                    [(ngModel)]="newAccountData.title+\' \'+newAccountData.firstname +\' \'+ newAccountData.lastname">\n\n                  </ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row>\n\n                <ion-col\n\n                  *ngIf="newAccountData.accountholdername !== null && newAccountData.accountholdername !== \'\' && newAccountData.accountholdername !== undefined">\n\n                  <ion-label fixed>{{settingsProvider.getLabel("Account Holder Name")}}</ion-label>\n\n                  <ion-input [disabled]="true" [(ngModel)]="newAccountData.accountholdername"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row\n\n                *ngIf="newAccountData.memberid !== null && newAccountData.memberid !== \'\' && newAccountData.memberid !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Account No\')}}</ion-label>\n\n                  <ion-input [disabled]="true" [(ngModel)]="newAccountData.memberid"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row\n\n                *ngIf="newAccountData.consectusid !== null && newAccountData.consectusid !== \'\' && newAccountData.consectusid !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Consectus ID\')}}</ion-label>\n\n                  <ion-input [disabled]="true" [(ngModel)]="newAccountData.consectusid"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row\n\n                *ngIf="newAccountData.customerid !== null && newAccountData.customerid !== \'\' && newAccountData.customerid !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Customer ID\')}}</ion-label>\n\n                  <ion-input [disabled]="true" [(ngModel)]="newAccountData.customerid"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row\n\n                *ngIf="newAccountData.mobileno !== null && newAccountData.mobileno !==\'\'&& newAccountData.mobileno !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Phone\')}}</ion-label>\n\n                  <ion-input [disabled]="true" name="gender" [(ngModel)]="newAccountData.mobileno">\n\n                  </ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row\n\n                *ngIf="newAccountData.productcode !== null && newAccountData.productcode !==\'\'&& newAccountData.productcode !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Product Code\')}}</ion-label>\n\n                  <ion-input [disabled]="true" name="gender" [(ngModel)]="newAccountData.productcode">\n\n                  </ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row\n\n                *ngIf="newAccountData.state_status !== null && newAccountData.state_status !==\'\' && newAccountData.state_status !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Status\')}}</ion-label>\n\n                  <ion-input [disabled]="true" name="gender" [(ngModel)]="newAccountData.state_status">\n\n                  </ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row\n\n                *ngIf="newAccountData.create_date !== null && newAccountData.create_date !== \'\' && newAccountData.create_date !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Account Created Date\')}}</ion-label>\n\n                  <ion-input [disabled]="true" name="date" [(ngModel)]="newAccountData.create_date">\n\n                  </ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n              <ion-row>\n\n                <ion-col>\n\n                  <!-- {{ this.newAccountData.state_status.indexOf(\'block\') }} {{ this.newAccountData.state_status}} -->\n\n                  <button ion-button round small color="primary"\n\n                    *ngIf="newAccountData.state_status==\'Existing Customer ID Blocked\' || newAccountData.state_status==\'New Existing Customer Blocked\' ||  newAccountData.state_status==\'New Existing Registration Blocked\' || newAccountData.state_status==\'Existing Blocked\' || newAccountData.state_status==\'Existing Registration Blocked\'|| newAccountData.state_status==\'New Active PIN Verification Blocked\' || newAccountData.state_status==\'Active PIN Verification Blocked\' || newAccountData.state_status==\'New Existing Mailer Blocked\' ||  newAccountData.state_status==\'New Existing Blocked\' ||  newAccountData.state_status==\'Existing Passcode Blocked\' ||  newAccountData.state_status==\'New Install Passcode Blocked\'"\n\n                    (click)="approveblocked(newAccountData.state_status)">{{settingsProvider.getLabel(\'Approve Blocked\')}}</button>\n\n                  <button ion-button round small color="primary"\n\n                    *ngIf=" newAccountData.state_status==\'Existing Mailer\' || newAccountData.state_status==\'New Existing Mailer\' || newAccountData.state_status==\'New Existing Mailer Blocked\' || newAccountData.state_status==\'Existing Mailer Blocked\'"\n\n                    (click)="unblockedMailer(newAccountData.state_status)">{{mailerButton}}</button>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-col>\n\n\n\n            <ion-col col-6>\n\n              <ion-row\n\n                *ngIf="newAccountData.jointholderfirstname && newAccountData.jointholderlastname != null || newAccountData.jointholderfirstname && newAccountData.jointholderlastname != \'\' || newAccountData.jointholderfirstname && newAccountData.jointholderlastname != undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Joint Account Holder Name\')}}</ion-label>\n\n                  <ion-input [disabled]="true"\n\n                    [(ngModel)]="newAccountData.jointholderfirstname +\' \'+newAccountData.jointholderlastname">\n\n                  </ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row\n\n                *ngIf="newAccountData.devicetype !== null && newAccountData.devicetype !== \'\' && newAccountData.devicetype !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Device Type\')}}</ion-label>\n\n                  <ion-input [disabled]="true" [(ngModel)]="newAccountData.devicetype"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row\n\n                *ngIf="newAccountData.houseno && newAccountData.street && newAccountData.city != null || newAccountData.houseno && newAccountData.street && newAccountData.city !=\'\'  || newAccountData.houseno && newAccountData.street && newAccountData.city != undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Address\')}}</ion-label>\n\n                  <ion-input [disabled]="true"\n\n                    [(ngModel)]="newAccountData.houseno+\' \'+ newAccountData.street+\' \'+newAccountData.city"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row\n\n                *ngIf="newAccountData.dob !== null && newAccountData.dob !== \'\'&& newAccountData.dob !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Date of Birth\')}}</ion-label>\n\n                  <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" [(ngModel)]="newAccountData.dob"\n\n                    [disabled]="true"></ion-datetime>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row\n\n                *ngIf="newAccountData.accountype !== null && newAccountData.accountype !== \'\' && newAccountData.accountype !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'Account Type\')}}</ion-label>\n\n                  <ion-input [disabled]="true" [(ngModel)]="newAccountData.accountype"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row\n\n                *ngIf="newAccountData.uuid !== null && newAccountData.uuid !== \'\' && newAccountData.uuid !== undefined">\n\n                <ion-col>\n\n                  <ion-label fixed>{{settingsProvider.getLabel(\'UUID\')}}</ion-label>\n\n                  <ion-input [disabled]="true" [(ngModel)]="newAccountData.uuid"></ion-input>\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row *ngIf="newAccountData.state_status ==\'New Existing Mailer Drafted\' ||\n\n                        newAccountData.state_status==\'New Customer Mailer Drafted\'">\n\n                <ion-col>\n\n                  <img type="file" class="imgActiveCust" style="width:40%;height:35px;"\n\n                    (click)="DownloadPdf(newAccountData)" src="assets/member_images/DownloadPdf.jpg" alt="">\n\n                </ion-col>\n\n              </ion-row>\n\n              <ion-row>\n\n                <ion-col margin style="padding: 10px;">\n\n                  <button *ngIf="newAccountData.accountstatus==\'Pending\'" ion-button round small color="primary"\n\n                    (click)="pending()">{{settingsProvider.getLabel(\'Pending\')}}</button>\n\n                  <button *ngIf="newAccountData.accountstatus==\'Rejected\'" ion-button round small color="danger"\n\n                    (click)="rejected()">{{settingsProvider.getLabel(\'Reject\')}}</button>\n\n                  <button *ngIf="newAccountData.accountstatus==\'Approved\'" ion-button round small color="primary"\n\n                    (click)="approved()">{{settingsProvider.getLabel(\'Approved\')}}</button>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-card>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <ion-card padding>\n\n          <ion-col col-12>\n\n            <ion-card-header>User Activity</ion-card-header>\n\n            <table-component [options]="newAccountActivityOptions"></table-component>\n\n          </ion-col>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>  \n\n\n\n\n\n\n\n\n\n\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\new-account-view\new-account-view.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_new_account_new_account__["a" /* NewAccountProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], NewAccountViewPage);

//# sourceMappingURL=new-account-view.js.map

/***/ })

});
//# sourceMappingURL=18.js.map