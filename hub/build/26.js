webpackJsonp([26],{

/***/ 571:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqCategoryDetailPageModule", function() { return FaqCategoryDetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__faq_category_detail__ = __webpack_require__(608);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FaqCategoryDetailPageModule = (function () {
    function FaqCategoryDetailPageModule() {
    }
    return FaqCategoryDetailPageModule;
}());
FaqCategoryDetailPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__faq_category_detail__["a" /* FaqCategoryDetailPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__faq_category_detail__["a" /* FaqCategoryDetailPage */]),
        ],
    })
], FaqCategoryDetailPageModule);

//# sourceMappingURL=faq-category-detail.module.js.map

/***/ }),

/***/ 608:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqCategoryDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_faq_category_faq_category__ = __webpack_require__(384);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FaqCategoryDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FaqCategoryDetailPage = (function () {
    function FaqCategoryDetailPage(navCtrl, navParams, alertCtrl, toastCtrl, faqCategoryProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.faqCategoryProvider = faqCategoryProvider;
        this.canUpdate = false;
        this.new = false;
        this.faqCategories = [];
        if (!this.navParams.get('category')) {
            this.faqCategories = {
                categoryname: "",
                createdDate: "",
                isactive: 1,
            };
            console.log(this.faqCategories, "faqsData");
            this.title = "Add FAQ Category";
            this.new = true;
        }
        else {
            this.faqCategories = JSON.parse(JSON.stringify(navParams.get('category')));
            this.title = "Update FAQ Category";
            this.canUpdate = true;
            // this.new = false;
            // this.title = "Update FAQ";
        }
    }
    FaqCategoryDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FaqCategoryDetailPage');
    };
    FaqCategoryDetailPage.prototype.dirty = function () {
        this.dirtyFlag = true;
    };
    FaqCategoryDetailPage.prototype.addFAQ = function () {
        var msg = [];
        if (this.faqCategories.categoryname <= 0)
            msg.push("Category name should be specified");
        if (msg.length > 0) {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                message: msg.join("<br />"),
                buttons: [
                    {
                        text: 'OK',
                        role: 'cancel',
                        handler: function (data) {
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var self_1 = this;
            this.faqCategoryProvider.addcategories(this.faqCategories)
                .then(function (data) {
                var toast = self_1.toastCtrl.create({
                    message: 'FAQ added successfully.',
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
                self_1.navCtrl.pop();
            });
        }
    };
    FaqCategoryDetailPage.prototype.updateFAQ = function () {
        var self = this;
        this.faqCategoryProvider.editcategory(this.faqCategories)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: "FAQ category updated successfully",
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        });
    };
    return FaqCategoryDetailPage;
}());
FaqCategoryDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-faq-category-detail',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\faq-category-detail\faq-category-detail.html"*/'<!--\n  Generated template for the FaqCategoryDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{title}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid class="table-cell">\n    <ion-row>\n      <ion-col>\n\n        <ion-row *ngIf="!canUpdate">\n          <ion-col>\n            <ion-toolbar>\n              <ion-buttons end>\n                <button ion-button small round icon-left color="primary" (click)="addFAQ()" *ngIf="dirtyFlag">\n                  <ion-icon name="checkmark-circle"></ion-icon>\n                  Save Changes\n                </button>\n              </ion-buttons>\n            </ion-toolbar>\n          </ion-col>\n        </ion-row>\n\n        <ion-row *ngIf="canUpdate">\n          <ion-col>\n            <ion-toolbar>\n              <ion-buttons end>\n                <button ion-button small round icon-left color="primary" (click)="updateFAQ()" *ngIf="dirtyFlag">\n                  <ion-icon name="checkmark-circle"></ion-icon>\n                  Save Changes\n                </button>\n              </ion-buttons>\n            </ion-toolbar>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col>\n            <ion-grid class="table-simple">\n\n              <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label stacked>Category Name</ion-label>\n                    <ion-input type="text" [(ngModel)]="faqCategories.categoryname" placeholder="Category Name" (input)="dirty()"></ion-input>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n\n              <!-- <ion-row>\n                <ion-col>\n                  <ion-item>\n                    <ion-label stacked>Created Date</ion-label>\n                    <ion-input type="tel" name="date" [(ngModel)]="faqCategories.created_date" placeholder="Created Date" (input)="dirty()"></ion-input>\n                  </ion-item>\n                </ion-col>\n              </ion-row> -->\n\n            </ion-grid>\n          </ion-col>\n        </ion-row>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\faq-category-detail\faq-category-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_faq_category_faq_category__["a" /* FaqCategoryProvider */]])
], FaqCategoryDetailPage);

//# sourceMappingURL=faq-category-detail.js.map

/***/ })

});
//# sourceMappingURL=26.js.map