webpackJsonp([5],{

/***/ 592:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionDetailsPageModule", function() { return TransactionDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__transaction_details__ = __webpack_require__(629);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TransactionDetailsPageModule = (function () {
    function TransactionDetailsPageModule() {
    }
    return TransactionDetailsPageModule;
}());
TransactionDetailsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__transaction_details__["a" /* TransactionDetailsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__transaction_details__["a" /* TransactionDetailsPage */]),
        ],
    })
], TransactionDetailsPageModule);

//# sourceMappingURL=transaction-details.module.js.map

/***/ }),

/***/ 629:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_transaction_transaction__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TransactionDetailsPage = (function () {
    function TransactionDetailsPage(navCtrl, navParams, oauthProvider, languageProvider, toastCtrl, transactionProvider, loadauditProvider, settingsProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.toastCtrl = toastCtrl;
        this.transactionProvider = transactionProvider;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.transactionData = {
            customer_account_transactions_id: "",
            create_date: "",
            amount: "",
            type: "",
            transferaccount: "",
            toAccount: "",
            status: ""
        };
        this.new = false;
        this.canUpdate = false;
        this.resultdata = null;
        this.languageString = "en";
        this.transactionList = navParams.get("transactionData");
        console.log(this.transactionList.status, "My transaction data");
        var self = this;
        if (!navParams.get('transactionData')) {
            //add adata
            self.transactionData = {
                "customer_account_transactions_id": null,
                "create_date": null,
                "amount": null,
                "type": null,
                "transferaccount": null,
                "toAccount": null,
                "status": null,
            };
            self.title = settingsProvider.getLabel("Update Transaction");
            self.new = true;
        }
        else {
            //View Data
            self.transactionData = JSON.parse(JSON.stringify(navParams.get('transactionData')));
            self.transactionData = self.transactionData;
            console.log("transactionData", this.transactionData);
            self.canUpdate = true;
            self.title = settingsProvider.getLabel("View Transaction");
        }
    }
    TransactionDetailsPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("transactions")) {
            return true;
        }
        else {
            return false;
        }
    };
    TransactionDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TransactionDetailsPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        var self = this;
        var message = "Viewed " + this.transactionData.customer_account_transactions_id + " in Transaction Details";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "transaction-details",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    TransactionDetailsPage.prototype.UpdateTransaction = function (status) {
        var _this = this;
        var self = this;
        console.log(status, "my status");
        var updatestatus = {
            "customer_account_transactions_id": this.transactionData.customer_account_transactions_id,
            "status": status
        };
        this.transactionProvider.updatecustomeraccounttransations(updatestatus)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: 'Updated Successfully.',
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        });
        var message = "Updated " + this.transactionData.customer_account_transactions_id + " details in Transaction Details";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "update",
            "screen": "transaction-details",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Updated Audit Done"),
                duration: 10000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    return TransactionDetailsPage;
}());
TransactionDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-transaction-details',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\transaction-details\transaction-details.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel("Transaction Details")}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-card padding>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel("Consectus ID")}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="transactionData.customer_account_transactions_id" placeholder="{{settingsProvider.getLabel(\'Customer Name\')}}" [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked> {{settingsProvider.getLabel("Date")}}</ion-label>\n\n              <ion-datetime type="text" displayFormat="MM/DD/YYYY" [(ngModel)]="transactionData.create_date" placeholder="{{settingsProvider.getLabel(\'Date\')}}" [disabled]="!new"></ion-datetime>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel("Amount")}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="transactionData.amount" placeholder="{{settingsProvider.getLabel(\'Amount\')}}"  [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel("Transfer Type")}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="transactionData.type" placeholder="{{settingsProvider.getLabel(\'Transfer Type\')}}"  [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel("From Account")}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="transactionData.transferaccount" placeholder="{{settingsProvider.getLabel(\'From Account\')}}"  [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-label stacked>{{settingsProvider.getLabel("Status")}}</ion-label>\n\n              <ion-input type="text" [(ngModel)]="transactionData.status" placeholder="{{settingsProvider.getLabel(\'Status\')}}"  [disabled]="!new"></ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row *ngIf="transactionData.status == \'pending\'">\n\n            <ion-col>\n\n              <button small ion-button (click)="UpdateTransaction(\'cleared\')">{{settingsProvider.getLabel("Cleared")}}</button>\n\n              <button small ion-button color="danger" (click)="UpdateTransaction(\'rejected\')">{{settingsProvider.getLabel("Rejected")}}</button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\transaction-details\transaction-details.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_transaction_transaction__["a" /* TransactionProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], TransactionDetailsPage);

//# sourceMappingURL=transaction-details.js.map

/***/ })

});
//# sourceMappingURL=5.js.map