webpackJsonp([22],{

/***/ 574:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LabelsPageModule", function() { return LabelsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__labels__ = __webpack_require__(611);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LabelsPageModule = (function () {
    function LabelsPageModule() {
    }
    return LabelsPageModule;
}());
LabelsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__labels__["a" /* LabelsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__labels__["a" /* LabelsPage */]),
        ],
    })
], LabelsPageModule);

//# sourceMappingURL=labels.module.js.map

/***/ }),

/***/ 611:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LabelsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LabelsPage = (function () {
    function LabelsPage(navCtrl, navParams, languageProvider, oauthProvider, toastCtrl, globalProvider, events, settingsProvider, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.toastCtrl = toastCtrl;
        this.globalProvider = globalProvider;
        this.events = events;
        this.settingsProvider = settingsProvider;
        this.masterpermissionProvider = masterpermissionProvider;
        this.currentTab = "hubLabels";
        this.languageString = "en";
        this.languageString1 = "en";
        this.resultdata1 = null;
        this.pageArr = [];
        this.stringLanguage = "";
    }
    LabelsPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("labels")) {
            return true;
        }
        else {
            return false;
        }
    };
    LabelsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.globalProvider.pageLoading();
        this.languageProvider.getLabels().then(function (data) {
            console.log("data", data);
            if (data.data !== undefined) {
                _this.labelsData = data.data;
                for (var key in data.data.hub) {
                    _this.pageArr.push(key);
                }
                _this.pageArr.sort();
                for (var key1 in data.data.hub.LabelsPage) {
                    if (key1 == "labels_" + _this.languageString) {
                        _this.resultdata1 = data.data.hub.LabelsPage[key1];
                    }
                }
            }
        });
    };
    LabelsPage.prototype.segmentChanged = function (event) {
        console.log(event);
        this.pageArr = [];
        switch (this.currentTab) {
            case "hubLabels":
                this.labelFormData = null;
                for (var key in this.labelsData.hub) {
                    this.pageArr.push(key);
                }
                this.pageArr.sort();
                break;
            case "appLabels":
                this.labelFormData = null;
                for (var key in this.labelsData.app) {
                    this.pageArr.push(key);
                }
                this.pageArr.sort();
                break;
        }
    };
    LabelsPage.prototype.showForm = function (page, pageScreen, index) {
        this.selectedBtn = index;
        this.labelFormData = this.labelsData[pageScreen][page];
        this.stringLanguage = "labels_" + this.languageString;
        this.keys = Object.keys(this.labelFormData["labels_" + this.languageString]);
        console.log("labelFormData", this.labelFormData["labels_" + this.languageString]);
    };
    LabelsPage.prototype.saveLabels = function () {
        var _this = this;
        var self = this;
        console.log("Language", this.labelFormData[this.stringLanguage]);
        this.globalProvider.pageLoading();
        this.languageProvider.saveLabels(this.labelFormData[this.stringLanguage], this.labelsData, "hub").then(function (result) {
            console.log("result", result);
            if (result.data !== undefined) {
                for (var key1 in result.data.hub.MenuPage) {
                    if (key1 == "labels_" + _this.languageString) {
                        _this.resultdata = result.data.hub.MenuPage[key1];
                    }
                    else if (key1 == "labels_en") {
                        _this.resultdata = result.data.hub.MenuPage[key1];
                    }
                }
            }
            self.events.publish("consectus:menu", _this.resultdata);
            self.events.publish("consectus:labels.updated");
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: "success"
            });
            toast.present();
        });
    };
    LabelsPage.prototype.addLabel = function () {
        var _this = this;
        var self = this;
        this.globalProvider.pageLoading();
        this.languageProvider.addLabel().then(function (result) {
            if (result.data !== undefined) {
                for (var key1 in result.data.hub.MenuPage) {
                    if (key1 == "labels_" + _this.languageString) {
                        _this.resultdata = result.data.hub.MenuPage[key1];
                    }
                    else if (key1 == "labels_en") {
                        _this.resultdata = result.data.hub.MenuPage[key1];
                    }
                }
            }
            self.events.publish("consectus:menu", _this.resultdata);
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: "success"
            });
            toast.present();
        });
    };
    LabelsPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    return LabelsPage;
}());
LabelsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-labels',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\labels\labels.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Labels Management | Consectus\')}} </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div padding>\n\n    <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n      <ion-segment-button value="hubLabels">\n\n        {{settingsProvider.getLabel("Hub Labels")}}\n\n      </ion-segment-button>\n\n      <ion-segment-button value="appLabels">\n\n        {{settingsProvider.getLabel("App Labels")}}\n\n      </ion-segment-button>\n\n    </ion-segment>\n\n\n\n    <button round ion-button small float-right margin-top margin-bottom round color="primary" (click)="addLabel()">\n\n      {{settingsProvider.getLabel("Add Label File")}}\n\n    </button>\n\n  </div>\n\n\n\n  <div *ngIf="currentTab == \'hubLabels\'">\n\n    <ion-card-content padding>\n\n      <ion-card padding>\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col col-3 *ngFor="let page of pageArr;  let i = index">\n\n              <button round ion-button small round color="light" [class.active]="i == selectedBtn" class="pageBtn" (click)="showForm(page,\'hub\',i)">\n\n                {{page}}\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-card>\n\n    </ion-card-content>\n\n    <ion-card-content padding>\n\n      <ion-card *ngIf="labelFormData">\n\n        <ion-grid>\n\n          <ion-grid>\n\n            <ion-row>\n\n              <ion-col col-4 *ngFor="let key of keys">\n\n                <ion-label fixed> {{key}}</ion-label>\n\n                <ion-input type="text" name="labels" [(ngModel)]="labelFormData[stringLanguage][key]">\n\n                </ion-input>\n\n              </ion-col>\n\n            </ion-row>\n\n            <ion-row text-center>\n\n              <button margin round ion-button small round color="primary" (click)="saveLabels()"> {{settingsProvider.getLabel("Update")}}\n\n              </button>\n\n            </ion-row>\n\n          </ion-grid>\n\n        </ion-grid>\n\n      </ion-card>\n\n    </ion-card-content>\n\n  </div>\n\n  <div *ngIf="currentTab == \'appLabels\'">\n\n    <ion-card-content padding>\n\n      <ion-card padding>\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col col-3 *ngFor="let page of pageArr;  let i = index">\n\n              <button round ion-button small class="pageBtn" [class.active]="i == selectedBtn" round color="light" (click)="showForm(page,\'app\', i)">\n\n                {{page}}\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-card>\n\n    </ion-card-content>\n\n    <ion-card-content padding>\n\n      <ion-card padding *ngIf="labelFormData">\n\n        <ion-grid>\n\n          <ion-grid>\n\n            <ion-row>\n\n              <ion-col col-4 *ngFor="let key of keys">\n\n                <ion-label fixed> {{key}}</ion-label>\n\n                <ion-input type="text" name="labels" [(ngModel)]="labelFormData[stringLanguage][key]">\n\n                </ion-input>\n\n              </ion-col>\n\n            </ion-row>\n\n            <ion-row text-center>\n\n              <button margin round ion-button small round color="primary" (click)="saveLabels()"> {{settingsProvider.getLabel("Update")}}\n\n              </button>\n\n            </ion-row>\n\n          </ion-grid>\n\n        </ion-grid>\n\n      </ion-card>\n\n    </ion-card-content>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\labels\labels.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], LabelsPage);

//# sourceMappingURL=labels.js.map

/***/ })

});
//# sourceMappingURL=22.js.map