webpackJsonp([2],{

/***/ 594:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAccessPageModule", function() { return UserAccessPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_access__ = __webpack_require__(631);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UserAccessPageModule = (function () {
    function UserAccessPageModule() {
    }
    return UserAccessPageModule;
}());
UserAccessPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__user_access__["a" /* UserAccessPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__user_access__["a" /* UserAccessPage */]),
        ],
    })
], UserAccessPageModule);

//# sourceMappingURL=user-access.module.js.map

/***/ }),

/***/ 631:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserAccessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_roles_user_roles__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var UserAccessPage = (function () {
    function UserAccessPage(navCtrl, navParams, events, oauthProvider, languageProvider, userRolesProvider, cdr, toastCtrl, viewCtrl, loadauditProvider, settingsProvider, alertCtrl, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.userRolesProvider = userRolesProvider;
        this.cdr = cdr;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.alertCtrl = alertCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.canUpdate = false;
        this.new = false;
        this.resultdata = null;
        this.languageString = "en";
        this.role = {
            user_roles_id: 1,
            name: "",
            staffusers: "",
            customers: "",
            products: "",
            pushmessages: "",
            members: "",
            appsettings: "",
            news: "",
            rulesengine: "",
            labels: "",
            newaccount: "",
            goal_milestone: "",
            transactions: "",
            calls: "",
            admin: "",
            isactive: ""
        };
        var self = this;
        if (!navParams.get('user')) {
            //add adata
            self.role = {
                "rolename": null,
                "staffusers": null,
                "customers": null,
                "products": null,
                "pushmessages": null,
                "members": null,
                "appsettings": null,
                "news": null,
                "rulesengine": null,
                "labels": null,
                "newaccount": null,
                "admin": null,
                "transactions": null,
                "calls": null,
                "goal_milestone": null,
                "createdby": null
            };
            this.usertitle = "Add User Roles";
            self.new = true;
            self.canUpdate = false;
        }
        else {
            //View Data
            this.usertitle = "Update User Roles";
            self.role = JSON.parse(JSON.stringify(navParams.get('user')));
            console.log(self.role);
            self.canUpdate = true;
        }
    }
    UserAccessPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("staffusers")) {
            return true;
        }
        else {
            return false;
        }
    };
    UserAccessPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UserAccessPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        var self = this;
        var message = "Viewed " + this.role.rolename + " in Transaction Details";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "user-access",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    UserAccessPage.prototype.toggleStaffusers = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.staffusers = true;
            }
            if (type == "false") {
                this.role.staffusers = false;
            }
        }
        else {
            if (type == "true") {
                this.role.staffusers = false;
            }
            if (type == "false") {
                this.role.staffusers = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleCustomers = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.customers = true;
            }
            if (type == "false") {
                this.role.customers = false;
            }
        }
        else {
            if (type == "true") {
                this.role.customers = false;
            }
            if (type == "false") {
                this.role.customers = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleProducts = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.products = true;
            }
            if (type == "false") {
                this.role.products = false;
            }
        }
        else {
            if (type == "true") {
                this.role.products = false;
            }
            if (type == "false") {
                this.role.products = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.togglePushmessages = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.pushmessages = true;
            }
            if (type == "false") {
                this.role.pushmessages = false;
            }
        }
        else {
            if (type == "true") {
                this.role.pushmessages = false;
            }
            if (type == "false") {
                this.role.pushmessages = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleMembers = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.members = true;
            }
            if (type == "false") {
                this.role.members = false;
            }
        }
        else {
            if (type == "true") {
                this.role.members = false;
            }
            if (type == "false") {
                this.role.members = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleAppsettings = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.appsettings = true;
            }
            if (type == "false") {
                this.role.appsettings = false;
            }
        }
        else {
            if (type == "true") {
                this.role.appsettings = false;
            }
            if (type == "false") {
                this.role.appsettings = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleNews = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.news = true;
            }
            if (type == "false") {
                this.role.news = false;
            }
        }
        else {
            if (type == "true") {
                this.role.news = false;
            }
            if (type == "false") {
                this.role.news = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleRulesengine = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.rulesengine = true;
            }
            if (type == "false") {
                this.role.rulesengine = false;
            }
        }
        else {
            if (type == "true") {
                this.role.rulesengine = false;
            }
            if (type == "false") {
                this.role.rulesengine = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleNewAccount = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.newaccount = true;
            }
            if (type == "false") {
                this.role.newaccount = false;
            }
        }
        else {
            if (type == "true") {
                this.role.newaccount = false;
            }
            if (type == "false") {
                this.role.newaccount = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleCalls = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.calls = true;
            }
            if (type == "false") {
                this.role.calls = false;
            }
        }
        else {
            if (type == "true") {
                this.role.calls = false;
            }
            if (type == "false") {
                this.role.calls = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleTransactions = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.transactions = true;
            }
            if (type == "false") {
                this.role.transactions = false;
            }
        }
        else {
            if (type == "true") {
                this.role.transactions = false;
            }
            if (type == "false") {
                this.role.transactions = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleMileStone = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.goal_milestone = true;
            }
            if (type == "false") {
                this.role.goal_milestone = false;
            }
        }
        else {
            if (type == "true") {
                this.role.goal_milestone = false;
            }
            if (type == "false") {
                this.role.goal_milestone = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleAdmin = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.admin = true;
            }
            if (type == "false") {
                this.role.admin = false;
            }
        }
        else {
            if (type == "true") {
                this.role.admin = false;
            }
            if (type == "false") {
                this.role.admin = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.toggleLabels = function (type, e) {
        if (e.checked) {
            if (type == "true") {
                this.role.labels = true;
            }
            if (type == "false") {
                this.role.labels = false;
            }
        }
        else {
            if (type == "true") {
                this.role.labels = false;
            }
            if (type == "false") {
                this.role.labels = true;
            }
        }
        this.cdr.detectChanges();
    };
    UserAccessPage.prototype.save = function () {
        var _this = this;
        var self = this;
        self.role = {
            "rolename": self.role.rolename,
            "staffusers": self.role.staffusers,
            "customers": self.role.customers,
            "products": self.role.products,
            "pushmessages": self.role.pushmessages,
            "members": self.role.members,
            "appsettings": self.role.appsettings,
            "news": self.role.news,
            "rulesengine": self.role.rulesengine,
            "labels": self.role.labels,
            "new_account": self.role.newaccount,
            "admin": self.role.admin,
            "transactions": self.role.transactions,
            "calls": self.role.calls,
            "goal_milestone": self.role.goal_milestone
        };
        if (this.role.rolename == null || this.role.rolename == undefined) {
            console.log(this.role.rolename, "accountname");
            this.errorAlert("Please enter Role Name");
        }
        else {
            this.userRolesProvider.addrole(JSON.parse(JSON.stringify(self.role)))
                .then(function (result) {
                result.rolename = "";
                result.staffusers = "";
                result.customers = "";
                result.products = "";
                result.pushmessages = "";
                result.members = "";
                result.appsettings = "";
                result.news = "";
                result.rulesengine = "";
                result.newaccount = "";
                result.transactions = "";
                result.calls = "";
                result.goal_milestone = "";
                result.admin = "";
                result.labels = "";
                result.isactive = "";
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel('User Role added successfully'),
                    duration: 3000,
                    position: 'right',
                    cssClass: "success"
                });
                toast.present();
                self.viewCtrl.dismiss();
            });
            var message = "added " + this.role.rolename + " in staff users ";
            var auditUser = {
                "users_id": this.oauthProvider.user[0].users_id,
                "customer_devices_id": 0,
                "usertype": "staff",
                "uuid": "",
                "appname": "hub",
                "event_type": "add",
                "screen": "user-access",
                "trail_details": message
            };
            this.loadauditProvider.loadaudit(auditUser)
                .then(function (data) {
                var toast = self.toastCtrl.create({
                    message: _this.settingsProvider.getLabel("Add Audit Done"),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
            });
        }
    };
    UserAccessPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    UserAccessPage.prototype.UpdateUserRole = function () {
        var _this = this;
        var self = this;
        if (!this.canUpdate) {
            var message_1 = "User Access Added Successfully";
        }
        else {
            var message_2 = "User Access Updated Successfully";
        }
        self.role = {
            "user_roles_id": self.role.user_roles_id,
            "rolename": self.role.rolename,
            "staffusers": self.role.staffusers ? 1 : 0,
            "customers": self.role.customers ? 1 : 0,
            "products": self.role.products ? 1 : 0,
            "pushmessages": self.role.pushmessages ? 1 : 0,
            "members": self.role.members ? 1 : 0,
            "appsettings": self.role.appsettings ? 1 : 0,
            "news": self.role.news ? 1 : 0,
            "rulesengine": self.role.rulesengine ? 1 : 0,
            "labels": self.role.labels ? 1 : 0,
            "newaccount": self.role.newaccount ? 1 : 0,
            "transactions": self.role.transactions ? 1 : 0,
            "calls": self.role.calls ? 1 : 0,
            "goal_milestone": self.role.goal_milestone ? 1 : 0,
            "admin": self.role.admin ? 1 : 0,
            "isactive": self.role.isactive
        };
        console.log(JSON.stringify(this.role));
        this.userRolesProvider.roleSetting(this.role)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel(data.message),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            self.navCtrl.pop();
        });
        var message = "Updated " + this.role.rolename + " details in Transaction Details";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "update",
            "screen": "user-access",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
            var toast = self.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Updated Audit Done"),
                duration: 10000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
        });
    };
    return UserAccessPage;
}());
UserAccessPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-user-access',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\user-access\user-access.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n        <ion-title>{{settingsProvider.getLabel(usertitle)}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n    <ion-grid>\n\n        <ion-row *ngIf="!canUpdate">\n\n            <ion-col>\n\n                <ion-toolbar>\n\n                    <ion-buttons end>\n\n                        <button ion-button small round icon-left color="primary" (click)="save()">\n\n                            <ion-icon name="checkmark-circle"></ion-icon>\n\n                            {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                        </button>\n\n                    </ion-buttons>\n\n                </ion-toolbar>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="canUpdate">\n\n            <ion-col>\n\n                <ion-toolbar>\n\n                    <ion-buttons end>\n\n                        <button ion-button small round icon-left color="primary" (click)="UpdateUserRole()">\n\n                            <ion-icon name="checkmark-circle"></ion-icon>\n\n                            {{settingsProvider.getLabel(\'Save Changes\')}}\n\n                        </button>\n\n                    </ion-buttons>\n\n                </ion-toolbar>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col>\n\n                <ion-card padding>\n\n                    <ion-label stacked> {{settingsProvider.getLabel(\'User Role Name\')}}</ion-label>\n\n                    <ion-input type="text" [(ngModel)]="role.rolename" placeholder="{{settingsProvider.getLabel(\'User Role Name\')}}"></ion-input>\n\n                </ion-card>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-card>\n\n            <ion-row>\n\n                <ion-col class="header" col-12 style="text-align:center;padding:10px 0 10px 0;font-size: 20px">\n\n                    {{settingsProvider.getLabel(\'Access Pages\')}}\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Staff Users")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.staffusers" (ionChange)="toggleStaffusers(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Customers")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.customers" (ionChange)="toggleCustomers(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Products")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.products" (ionChange)="toggleProducts(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Push Messages")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.pushmessages" (ionChange)="togglePushmessages(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Members")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.members" (ionChange)="toggleMembers(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("App Settings")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.appsettings" (ionChange)="toggleAppsettings(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Labels")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.labels" (ionChange)="toggleLabels(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Rules Engine")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.rulesengine" (ionChange)="toggleRulesengine(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("New Account")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.newaccount" (ionChange)="toggleNewAccount(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Transactions")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.transactions" (ionChange)="toggleTransactions(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n\n\n                <!-- <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Goal MileStone")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.goal_milestone" (ionChange)="toggleMileStone(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n            </ion-col> -->\n\n\n\n                <ion-col col-4>\n\n                    <ion-item>\n\n                        <ion-label>{{settingsProvider.getLabel("Super Admin")}}</ion-label>\n\n                        <ion-checkbox [checked]="role.admin" (ionChange)="toggleAdmin(\'true\',$event)"></ion-checkbox>\n\n                    </ion-item>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n        </ion-card>\n\n\n\n\n\n    </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\user-access\user-access.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_roles_user_roles__["a" /* UserRolesProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], UserAccessPage);

//# sourceMappingURL=user-access.js.map

/***/ })

});
//# sourceMappingURL=2.js.map