webpackJsonp([17],{

/***/ 578:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewAccountPageModule", function() { return NewAccountPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__new_account__ = __webpack_require__(615);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NewAccountPageModule = (function () {
    function NewAccountPageModule() {
    }
    return NewAccountPageModule;
}());
NewAccountPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__new_account__["a" /* NewAccountPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__new_account__["a" /* NewAccountPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], NewAccountPageModule);

//# sourceMappingURL=new-account.module.js.map

/***/ }),

/***/ 615:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewAccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_new_account_new_account__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NewAccountPage = (function () {
    function NewAccountPage(navCtrl, navParams, oauthProvider, languageProvider, events, cdr, alertCtrl, toastCtrl, globalProvider, newAccountProvider, settingsProvider, viewCtrl, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.languageProvider = languageProvider;
        this.events = events;
        this.cdr = cdr;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.globalProvider = globalProvider;
        this.newAccountProvider = newAccountProvider;
        this.settingsProvider = settingsProvider;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.currentTab = "newAccount";
        this.newAccountOptions = {
            data: [],
            filteredData: [],
            columns: {
                // "consectusid": { title: "Consectus ID", search: true, col: 1 },
                "memberid": { title: "Customer ID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "accountno": { title: "Account No.", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "firstname": { title: "First Name", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "lastname": { title: "Last Name", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "mobileno": { title: "Mobile No.", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "devicetype": { title: "Device Type", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "dob": { title: "DOB", format: 'function', fn: this.formatDateTime.bind(this), search: true, col: 1 },
                "emailid": { title: "Email ID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "shortuuid": { title: "UUID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "lastaccess": { title: "Date", format: 'function', fn: this.formatDateTimeDate.bind(this), search: true, col: 1 },
                "state_status": { title: "Status", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
            },
            pagination: true,
            filter: null,
            search: true,
            select: false,
            selectedActions: [
                {
                    title: "Rejected",
                    action: this.rejected.bind(this),
                    color: 'danger'
                },
                {
                    title: "Approve",
                    action: this.approved.bind(this),
                    color: 'primary'
                },
                {
                    title: "Pending",
                    action: this.pending.bind(this),
                    color: 'light'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editNewAccount.bind(this),
            uploadOptions: {},
        };
        this.newAccountopeningOptions = {
            data: [],
            filteredData: [],
            columns: {
                "memberid": { title: "Customer Id", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                // "accountno": { title: "AFR", search: true, col: 1 },
                "firstname": { title: "First Name", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "lastname": { title: "Last Name", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "mobileno": { title: "Mobile No.", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "devicetype": { title: "Device Type", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "productcode": { title: "Product Code", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                // "productcode": { title: "Product Code", format: 'function', search: true, col: 1 },
                "postalcode": { title: "Postal Code", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "dob": { title: "DOB", format: 'function', fn: this.formatDateTime.bind(this), search: true, col: 1 },
                "emailid": { title: "Email ID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "shortuuid": { title: "UUID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "lastaccess": { title: "Date", format: 'function', fn: this.formatDateTimeDate.bind(this), search: true, col: 1 },
                "state_status": { title: "Status", format: 'function', fn: this.commonFunctionActive.bind(this), search: true },
            },
            pagination: true,
            filter: null,
            search: true,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editNewAccount.bind(this),
            uploadOptions: {},
        };
        this.accountMailerOptions = {
            data: [],
            filteredData: [],
            columns: {
                "memberid": { title: "Customer ID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "accountno": { title: "Account No.", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "firstname": { title: "First Name", search: true, mobile: true, format: 'function', fn: this.commonFunction.bind(this) },
                "lastname": { title: "Last Name", search: true, format: 'function', fn: this.commonFunction.bind(this) },
                "mobileno": { title: "Mobile No.", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "devicetype": { title: "Device Type", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "dob": { title: "DOB", format: 'function', fn: this.formatDateTime.bind(this), search: true, col: 1 },
                "shortuuid": { title: "UUID", search: true, col: 1, format: 'function', fn: this.commonFunction.bind(this) },
                "lastaccess": { title: "Date", format: 'function', fn: this.formatDateTime.bind(this), search: true, col: 1 },
                "state_status": { title: "Status", search: true, col: 1 },
                "mailer_status": { title: "Mailer Status", format: 'function', fn: this.commonFunctionSystemDfined.bind(this), search: true, col: 1 }
            },
            pagination: true,
            filter: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Generate Letter",
                    action: this.letter.bind(this),
                    color: 'success'
                },
                // {
                //   title: "Open Letter",
                //   action: this.openLetter.bind(this),
                //   color: 'success'
                // },
                {
                    title: "Send Letter",
                    action: this.letterDelivered.bind(this),
                    color: 'success'
                }
            ],
            allActions: [
                {
                    title: "Download All Generated Letters",
                    action: this.allLetters.bind(this),
                    color: 'success'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.mailerAccount.bind(this),
            uploadOptions: {},
        };
        this.resultdata = null;
        this.languageString = "en";
        // let testsdate = new Date("2018-11-06T07:35:42.000Z");
    }
    NewAccountPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("newaccount")) {
            return true;
        }
        else {
            return false;
        }
    };
    NewAccountPage.prototype.commonFunction = function (nullData) {
        var str = "";
        if (nullData == 0) {
            str = '--';
        }
        else if (nullData == null) {
            str = '--';
        }
        else if (nullData == undefined) {
            str = '--';
        }
        else {
            str = nullData;
        }
        return (str);
        // if (nullData == "New Existing Mailer Drafted" || nullData == "Existing Mailer Drafted" || nullData == "New Customer Mailer Drafted") {
        //   str = nullData//'<img type="file" class="imgActiveCust" src ="assets/member_images/DownloadPdf.jpg" alt="">';
        // } else {
        //   str = nullData
        // }
        // return (str);
    };
    // commonStatus(nullData) {
    //   let str = "";
    //   if (nullData == "New Existing Mailer Drafted" || nullData == "Existing Mailer Drafted" || nullData == "New Customer Mailer Drafted") {
    //     str = '<img type="file" class="imgActiveCust" src ="assets/member_images/DownloadPdf.jpg" alt="">';
    //   } else {
    //     str = nullData
    //   }
    //   return (str);
    // }
    NewAccountPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        // if (this.currentTab == "accountMailer") {
        //   this.getnewaccountmailerlist();
        // } else if (this.currentTab == "newAccountopening") {
        //   this.getNewAccountList();
        // }
        // let self = this;
        // if (!this.oauthProvider.user) {
        //   this.navCtrl.setRoot("LoginPage");
        // }
        // if (self.oauthProvider.user_role.newaccount) {
        //   self.getNewAccountList();
        // } else {
        //   let toast = self.toastCtrl.create({
        //     message: 'You are not authorised to access this page',
        //     duration: 4000,
        //     position: 'right',
        //     cssClass: 'success',
        //   })
        //   toast.present();
        //   self.navCtrl.setRoot("DashboardPage");
        // }
        var self = this;
        console.log("frrfnrfnerknrk lenght ", Object.keys(self.oauthProvider.user_role).length);
        if ((Object.keys(self.oauthProvider.user_role).length) !== 0) {
            if (this.currentTab == "accountMailer") {
                this.getnewaccountmailerlist();
            }
            else if (this.currentTab == "newAccountopening") {
                this.getNewAccountList();
            }
            else {
                //  if (this.currentTab == "newAccount") 
                this.getNewAccountLista();
            }
        }
        else {
            console.log(self.oauthProvider.user_role);
            self.navCtrl.setRoot("LoginPage");
        }
    };
    NewAccountPage.prototype.allLetters = function (action) {
        console.log(action);
        // let self = this;
        this.newAccountProvider.generateletterforonboard().then(function (result) {
            console.log(result.data);
            var data = result.data;
            var binary = atob(data.replace(/\s/g, ''));
            var len = binary.length;
            var buffer = new ArrayBuffer(len);
            var view = new Uint8Array(buffer);
            var j;
            for (j = 0; j < len; j++) {
                view[j] = binary.charCodeAt(j);
            }
            // create the blob object with content-type "application/pdf"               
            var url = URL.createObjectURL(new Blob([view], { type: "application/pdf" }));
            window.open(url);
        });
    };
    NewAccountPage.prototype.letter = function (new_accounts_id) {
        var _this = this;
        var d = [];
        var self = this;
        for (var _i = 0, new_accounts_id_1 = new_accounts_id; _i < new_accounts_id_1.length; _i++) {
            var item = new_accounts_id_1[_i];
            d.push(item.new_accounts_id);
        }
        var newaccntid = d.join(',');
        self.newAccountProvider.generateLetter(newaccntid)
            .then(function (result) {
            console.log("Letter Response" + JSON.stringify(result));
            var alert = _this.alertCtrl.create({
                title: 'Letter Generation Alert',
                subTitle: result.status == true ? "Letter generated only for the user's with status 'New Customer Mailer Input' and 'Existing Mailer'" : "Letter generation failed!",
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    // downloadFile(blob, fileName) {
    //   const link = document.createElement('a');
    //   // create a blobURI pointing to our Blob
    //   link.href = URL.createObjectURL(blob);
    //   link.download = fileName;
    //   // some browser needs the anchor to be in the doc
    //   document.body.append(link);
    //   link.click();
    //   link.remove();
    //   // in case the Blob uses a lot of memory
    //   window.addEventListener('focus', e => URL.revokeObjectURL(link.href));
    // };
    NewAccountPage.prototype.openLetter = function (new_accounts_id) {
        var _this = this;
        var d = [];
        var self = this;
        for (var _i = 0, new_accounts_id_2 = new_accounts_id; _i < new_accounts_id_2.length; _i++) {
            var item = new_accounts_id_2[_i];
            d.push(item.new_accounts_id);
        }
        var newaccntid = d.join(',');
        self.newAccountProvider.openPdfLetter(newaccntid)
            .then(function (result) {
            var data = result.data;
            var i;
            // let base64Str: any;
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                i = data_1[_i];
                if (i.generated_letter != null) {
                    var binary = atob(i.generated_letter.replace(/\s/g, ''));
                    var len = binary.length;
                    var buffer = new ArrayBuffer(len);
                    var view = new Uint8Array(buffer);
                    var j = void 0;
                    for (j = 0; j < len; j++) {
                        view[j] = binary.charCodeAt(j);
                    }
                    // create the blob object with content-type "application/pdf"               
                    self.generateDownload(new Blob([view], { type: "application/pdf" }));
                }
                else {
                    console.log("no base64 data available ...");
                }
            }
            var alert = _this.alertCtrl.create({
                title: 'Letter Generation Alert',
                subTitle: result.msg,
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    NewAccountPage.prototype.letterDelivered = function (data) {
        var _this = this;
        console.log(data);
        var newaccntid = [];
        for (var i = 0; i < data.length; i++) {
            var obj = {
                "new_accounts_id": data[i].new_accounts_id,
                "state_status": data[i].state_status.substring(0, data[i].state_status.length - 8) + " Send"
            };
            newaccntid.push(obj);
        }
        console.log(newaccntid);
        var self = this;
        this.newAccountProvider.sendLetterMailer("/hub/changeletterstatus", newaccntid).
            then(function (data) {
            var alert = _this.alertCtrl.create({
                title: 'Status has been updated successfully',
                buttons: ['Dismiss']
            });
            alert.present();
        }).catch(function (err) {
        });
    };
    NewAccountPage.prototype.generateDownload = function (base64Str) {
        var url = URL.createObjectURL(base64Str);
        window.addEventListener('focus', function (e) { return URL.revokeObjectURL(url); });
    };
    NewAccountPage.prototype.segmentChanged = function (event) {
        console.log("event", event._value);
        switch (event._value) {
            case 'newAccountopening':
                this.getNewAccountLista();
                break;
            case 'newAccount':
                this.getNewAccountList();
                break;
            case 'accountMailer':
                this.getnewaccountmailerlist();
                break;
            default:
                break;
        }
    };
    NewAccountPage.prototype.ionViewDidLoad = function () {
        // this.getNewAccountLista();
        this.getNewAccountList();
        // this.getnewaccountmailerlist();
    };
    NewAccountPage.prototype.getNewAccountList = function () {
        var self = this;
        self.newAccountProvider.getnewaccountslist().then(function (result) {
            self.newAccountOptions.data = result.data;
            self.newAccountOptions = Object.assign({}, self.newAccountOptions);
            self.cdr.detectChanges();
            self.nullData = result.data;
        });
    };
    NewAccountPage.prototype.getNewAccountLista = function () {
        var self = this;
        self.newAccountProvider.getnewaccountslist().then(function (result) {
            self.newAccountopeningOptions.data = result.data;
            self.newAccountopeningOptions = Object.assign({}, self.newAccountopeningOptions);
            self.cdr.detectChanges();
            self.nullDataActive = result.data;
        });
    };
    NewAccountPage.prototype.getnewaccountmailerlist = function () {
        var self = this;
        self.newAccountProvider.getnewaccountmailerlist("onboarding").then(function (result) {
            self.accountMailerOptions.data = result.data;
            self.accountMailerOptions = Object.assign({}, self.accountMailerOptions);
            self.cdr.detectChanges();
            self.nullData = result.data;
        });
    };
    NewAccountPage.prototype.formatDateTime = function (date) {
        var str = "";
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            // newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
        else {
            str = '--';
            return (str);
        }
    };
    NewAccountPage.prototype.formatDateTimeDate = function (date) {
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            var hh = newdate.getHours();
            var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            // newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
    };
    NewAccountPage.prototype.editNewAccount = function (newAccountData) {
        // if (newAccountData.state_status == "New Customer Active" || newAccountData.state_status == "New Existing Active" || newAccountData.state_status == "Existing Active" || newAccountData.state_status == "Active" && this.nullDataActive.customer_id != "") {
        //   this.navCtrl.push("CustomerPage", { customer: newAccountData });
        // } else {
        this.navCtrl.push("NewAccountViewPage", { newAccountData: newAccountData });
        // }
    };
    NewAccountPage.prototype.mailerAccount = function (newAccountData) {
        var _this = this;
        this.globalProvider.showLoader();
        setTimeout(function () {
            _this.navCtrl.push("NewAccountViewPage", { newAccountData: newAccountData });
            _this.globalProvider.hideLoader();
        }, 1000);
        // if (newAccountData.state_status == "New Existing Mailer Drafted" || newAccountData.state_status == "New Customer Mailer Drafted") {
        //   if (newAccountData.generated_letter == undefined || newAccountData.generated_letter == null) {
        //   }
        //   else {
        //     let binary = atob(newAccountData.generated_letter.replace(/\s/g, ''));
        //     let len = binary.length;
        //     let buffer = new ArrayBuffer(len);
        //     let view = new Uint8Array(buffer);
        //     let j: any;
        //     for (j = 0; j < len; j++) {
        //       view[j] = binary.charCodeAt(j);
        //     }
        //     // create the blob object with content-type "application/pdf"          
        //     let url = URL.createObjectURL(new Blob([view], { type: "application/pdf" }));
        //     window.open(url);
        //   }
        // } else {
        //   this.navCtrl.push("NewAccountViewPage", { newAccountData: newAccountData });
        // }
    };
    NewAccountPage.prototype.approved = function (items) {
        var d = [];
        var self = this;
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            d.push(item.newaccounts_id);
        }
        var customerIds = d.join(',');
        self.commonStatusFunction(customerIds, "In Progress");
    };
    NewAccountPage.prototype.rejected = function (items) {
        var d = [];
        var self = this;
        for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
            var item = items_2[_i];
            d.push(item.newaccounts_id);
        }
        var customerIds = d.join(',');
        self.commonStatusFunction(customerIds, "Rejected");
    };
    NewAccountPage.prototype.pending = function (items) {
        var d = [];
        var self = this;
        for (var _i = 0, items_3 = items; _i < items_3.length; _i++) {
            var item = items_3[_i];
            d.push(item.newaccounts_id);
        }
        var customerIds = d.join(',');
        self.commonStatusFunction(customerIds, "Pending");
    };
    NewAccountPage.prototype.commonStatusFunction = function (customerIds, status) {
        var self = this;
        self.newAccountProvider.updatenewaccountstatus(customerIds, status).then(function (result) {
            if (result.status) {
                self.getNewAccountList();
                var toast = self.toastCtrl.create({
                    message: self.settingsProvider.getLabel(result.message),
                    duration: 3000,
                    position: 'right',
                    cssClass: 'success',
                });
                toast.present();
            }
        });
    };
    NewAccountPage.prototype.commonFunctionSystemDfined = function (data) {
        var str = "";
        if (data == "Existing Mailer" || data == "New Existing Mailer") {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/close.jpg" alt="">';
        }
        else if (data == "New Customer Mailer Send" || data == "New Existing Mailer Send" || data == "Existing Mailer Send") {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/1398911.png" alt="">';
        }
        else {
            str = '<img class="imgUpload"  style="width:20%; height:20%" src ="assets/member_images/close.jpg" alt="">';
        }
        return (str);
    };
    NewAccountPage.prototype.commonFunctionActive = function (nullDataActive) {
        var str = "";
        if (nullDataActive == "New Customer Active" || nullDataActive == "New Existing Active" || nullDataActive == "Existing Active" || nullDataActive == "Active") {
            str = '<img type="file" class="imgActiveCust" src ="assets/member_images/viewCustomer.jpg"  (click)="myFunction()" alt="">';
        }
        else {
            str = nullDataActive;
        }
        return (str);
    };
    return NewAccountPage;
}());
NewAccountPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["n" /* Component */])({
        selector: 'page-new-account',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\new-account\new-account.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n        <ion-title padding-vertical>{{settingsProvider.getLabel("Onboarding Management | Consectus")}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <div>\n\n        <ion-row>\n\n            <fieldset style="width: 15%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>UUID:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Universally Unique IDentifier</label>\n\n                <br />\n\n            </fieldset>\n\n\n\n            <!-- <fieldset style="width: 15%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>OS:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Onboarding Status</label>\n\n                <br/>\n\n            </fieldset> -->\n\n            <!-- <fieldset style="width: 15%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>AFR:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Account Reference Number</label>\n\n                <br/>\n\n            </fieldset> -->\n\n            <!-- <fieldset style="padding: 25px;width: 11%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>FN:</legend>\n\n                <label for="kraken" style="font-size: 13px;">First Name</label>\n\n                <br/>\n\n            </fieldset>\n\n            <fieldset style="padding: 25px;width: 11%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>LN:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Last Name</label>\n\n                <br/>\n\n            </fieldset>\n\n            <fieldset style="padding: 25px;width: 11%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>AT:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Account Type</label>\n\n                <br/>\n\n            </fieldset>\n\n\n\n            <fieldset style="padding: 25px;width: 11%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>MD:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Modified Date</label>\n\n                <br/>\n\n            </fieldset> -->\n\n\n\n            <!-- <fieldset style="padding: 25px;width: 12%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>CT:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Customer Type</label>\n\n                <br/>\n\n            </fieldset> -->\n\n\n\n            <!-- <fieldset style="padding: 25px;width: 14%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>PEID:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Personal EID Status</label>\n\n                <br/>\n\n            </fieldset>\n\n\n\n            <fieldset style="padding: 25px;width: 17%; margin-top: 5px; margin-bottom: 5px;height: 64px;">\n\n                <legend>CEID:</legend>\n\n                <label for="kraken" style="font-size: 13px;">Current Account EID Status</label>\n\n                <br/>\n\n            </fieldset> -->\n\n        </ion-row>\n\n\n\n    </div>\n\n\n\n    <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n\n        <ion-segment-button value="newAccount">\n\n            {{ settingsProvider.getLabel(\'Onboarding Customers\')}}\n\n        </ion-segment-button>\n\n        <!-- <ion-segment-button value="newAccountopening">\n\n            {{ settingsProvider.getLabel(\'New Account Registration\')}}\n\n        </ion-segment-button> -->\n\n        <ion-segment-button value="accountMailer">\n\n            {{settingsProvider.getLabel(\'Account Mailer\')}}\n\n        </ion-segment-button>\n\n    </ion-segment>\n\n\n\n    <!-- <ion-grid class="table-cell" *ngIf="currentTab == \'newAccountopening\'">\n\n        <ion-row>\n\n            <ion-col>\n\n                <ion-grid class="table-simple">\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <table-component [options]="newAccountopeningOptions"></table-component>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-grid>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid> -->\n\n\n\n    <ion-grid class="table-cell" *ngIf="currentTab == \'newAccount\'">\n\n        <ion-row>\n\n            <ion-col>\n\n                <ion-grid class="table-simple">\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <table-component [options]="newAccountOptions"></table-component>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-grid>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n\n\n    <ion-grid class="table-cell" *ngIf="currentTab == \'accountMailer\'">\n\n        <ion-row>\n\n            <ion-col>\n\n                <ion-grid class="table-simple">\n\n                    <ion-row>\n\n                        <ion-col>\n\n                            <table-component [options]="accountMailerOptions"></table-component>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-grid>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\new-account\new-account.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* Events */],
        __WEBPACK_IMPORTED_MODULE_3__angular_core__["k" /* ChangeDetectorRef */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_new_account_new_account__["a" /* NewAccountProvider */],
        __WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], NewAccountPage);

//# sourceMappingURL=new-account.js.map

/***/ })

});
//# sourceMappingURL=17.js.map