webpackJsonp([29],{

/***/ 566:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function() { return ContactUsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us__ = __webpack_require__(603);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContactUsPageModule = (function () {
    function ContactUsPageModule() {
    }
    return ContactUsPageModule;
}());
ContactUsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */]),
        ],
    })
], ContactUsPageModule);

//# sourceMappingURL=contact-us.module.js.map

/***/ }),

/***/ 603:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactUsPage = (function () {
    function ContactUsPage(navCtrl, navParams, languageProvider, settingsProvider, oauthProvider, global, toastCtrl, alertCtrl, viewCtrl, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.settingsProvider = settingsProvider;
        this.oauthProvider = oauthProvider;
        this.global = global;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.contactus = null;
        this.stringData = "";
        this.resultdata = null;
        this.languageString = "en";
        this.canUpdate = false;
        this.new = false;
        this.contactus = this.navParams.get("contactUsData");
        console.log("contacts", this.contactus);
        if (this.contactus == undefined) {
            this.contactus = {
                subject: "",
                emailid: ""
            };
            this.stringData = "Save";
            this.title = "Add Contact Us Message Subject";
            this.new = true;
        }
        else {
            this.title = "Update Contact Us Message Subject";
            this.canUpdate = true;
            this.stringData = "Update";
        }
    }
    ContactUsPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("appsettings")) {
            return true;
        }
        else {
            return false;
        }
    };
    ContactUsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactUsPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    ContactUsPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
    };
    ContactUsPage.prototype.save = function () {
        var _this = this;
        var self = this;
        if (self.contactus.subject == "") {
            this.errorAlert("Please enter correct subject");
        }
        else if (self.contactus.emailid == "") {
            this.errorAlert("Please enter correct email Id");
        }
        else {
            if (this.canUpdate) {
                // self.global.showLoader();
                self.settingsProvider.updateContactSubject(this.contactus).then(function (result) {
                    console.log("result", result);
                    // self.global.hideLoader();
                    if (result.status) {
                        var toast = self.toastCtrl.create({
                            message: _this.settingsProvider.getLabel(result.message),
                            duration: 3000,
                            position: 'right',
                            cssClass: 'success',
                        });
                        toast.present();
                        self.navCtrl.pop();
                    }
                    else {
                        // self.global.hideLoader();
                        var toast = self.toastCtrl.create({
                            message: _this.settingsProvider.getLabel(result.message),
                            duration: 3000,
                            position: 'right',
                            cssClass: 'error',
                        });
                        toast.present();
                        self.navCtrl.pop();
                    }
                }).catch(function (error) {
                    // self.global.hideLoader();
                    console.log("error", error);
                });
            }
            else {
                // self.global.showLoader();
                self.settingsProvider.addcontactsubject(this.contactus).then(function (result) {
                    // self.global.hideLoader();
                    if (result.status) {
                        var toast = self.toastCtrl.create({
                            message: _this.settingsProvider.getLabel(result.message),
                            duration: 3000,
                            position: 'right',
                            cssClass: 'success',
                        });
                        toast.present();
                        self.navCtrl.pop();
                    }
                    else {
                        // self.global.hideLoader();
                        var toast = self.toastCtrl.create({
                            message: _this.settingsProvider.getLabel(result.message),
                            duration: 3000,
                            position: 'right',
                            cssClass: 'error',
                        });
                        toast.present();
                        self.navCtrl.pop();
                    }
                }).catch(function (error) {
                    // self.global.hideLoader();
                    console.log("error", error);
                });
            }
        }
    };
    ContactUsPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: this.settingsProvider.getLabel(message),
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return ContactUsPage;
}());
ContactUsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact-us',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\contact-us\contact-us.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-grid>\n\n\n\n    <ion-row *ngIf="canUpdate">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="save()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row *ngIf="!canUpdate">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="save()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-grid class="table-cell">\n\n          <ion-row>\n\n            <ion-col>\n\n              <ion-grid class="table-simple">\n\n                <ion-row>\n\n                  <ion-col col-6>\n\n                    <ion-item>\n\n                      <ion-label stacked>{{settingsProvider.getLabel("Subject")}}</ion-label>\n\n                      <ion-input type="text" [(ngModel)]="contactus.subject" placeholder="{{settingsProvider.getLabel(\'Subject\')}}"></ion-input>\n\n                    </ion-item>\n\n                  </ion-col>\n\n                  <ion-col col-6>\n\n                    <ion-item>\n\n                      <ion-label stacked>{{settingsProvider.getLabel("Email Id")}}</ion-label>\n\n                      <ion-input type="email" [(ngModel)]="contactus.emailid" placeholder="{{settingsProvider.getLabel(\'Email Id\')}}"></ion-input>\n\n                    </ion-item>\n\n                  </ion-col>\n\n                </ion-row>\n\n              </ion-grid>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\contact-us\contact-us.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], ContactUsPage);

//# sourceMappingURL=contact-us.js.map

/***/ })

});
//# sourceMappingURL=29.js.map