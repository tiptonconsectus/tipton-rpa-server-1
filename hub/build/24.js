webpackJsonp([24],{

/***/ 572:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqsPageModule", function() { return FaqsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__faqs__ = __webpack_require__(609);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FaqsPageModule = (function () {
    function FaqsPageModule() {
    }
    return FaqsPageModule;
}());
FaqsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__faqs__["a" /* FaqsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__faqs__["a" /* FaqsPage */]),
        ],
    })
], FaqsPageModule);

//# sourceMappingURL=faqs.module.js.map

/***/ }),

/***/ 609:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_masterpermission_masterpermission__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FaqsPage = (function () {
    function FaqsPage(navCtrl, navParams, settingsProvider, languageProvider, oauthProvider, toastCtrl, alertCtrl, viewCtrl, masterpermissionProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.settingsProvider = settingsProvider;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.masterpermissionProvider = masterpermissionProvider;
        this.title = "FAQs ?";
        this.faqsData = {
            title: "",
            description: "",
            category: "",
            type: "mobile"
        };
        this.new = true;
        this.resultdata = null;
        this.languageString = "en";
        this.categorylist = null;
        this.selectedcategory = "";
        this.selectedcategoryname = "selectedcategoryname";
        console.log(this.navParams.get('faq'));
        if (this.navParams.get('faq') == null) {
            this.faqsData = {
                title: "",
                description: "",
                category: "app",
                type: "mobile"
            };
            console.log(this.faqsData, "faqsData");
            this.title = "Add FAQ";
            this.new = true;
        }
        else {
            this.faqsData = JSON.parse(JSON.stringify(navParams.get('faq')));
            this.selectedcategoryname = this.faqsData.category;
            this.selectedcategory = this.faqsData.category;
            this.new = false;
            this.title = "Update FAQ";
        }
    }
    FaqsPage.prototype.ionViewCanEnter = function () {
        // here we can either return true or false
        // depending on if we want to leave this view
        if (this.masterpermissionProvider.permission("appsettings")) {
            return true;
        }
        else {
            return false;
        }
    };
    FaqsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FaqsPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        else {
            var self_1 = this;
            this.settingsProvider.getAllFaqCategories()
                .then(function (categoryjson) {
                var output = [];
                console.log(categoryjson.data);
                // for (let i = 0; i < categoryjson.data.length; i++) {
                // }
                self_1.categorylist = categoryjson.data.filter(function (x) { return x.isactive === 1; });
                console.log(self_1.faqsData, "sfsajfasfhasjfkasgfagfgffa");
                if (self_1.faqsData.category != null || self_1.faqsData.category == undefined || self_1.faqsData.category == "") {
                    self_1.selectedcategoryname = self_1.faqsData.category;
                    self_1.selectedcategory = self_1.faqsData.category;
                }
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    FaqsPage.prototype.ionViewDidEnter = function () {
        if (this.faqsData.category != null || this.faqsData.category == undefined || this.faqsData.category == "") {
            this.selectedcategoryname = this.faqsData.category;
            this.selectedcategory = this.faqsData.category;
        }
    };
    FaqsPage.prototype.setCategory = function () {
    };
    FaqsPage.prototype.selectCategory = function (data) {
        if (data != null) {
            //this.selectedcategory = data;
            this.selectedcategoryname = data;
            this.faqsData.category = data;
        }
    };
    FaqsPage.prototype.addFaqs = function () {
        var self = this;
        if (self.faqsData.title == "" || self.faqsData.title.trim() == "") {
            this.errorAlert(this.settingsProvider.getLabel("Please enter title"));
        }
        else if (self.faqsData.description == "" || self.faqsData.description.trim() == "") {
            this.errorAlert(this.settingsProvider.getLabel("Please enter description"));
        }
        else if (self.faqsData.category == "" || self.faqsData.category.trim() == "") {
            this.errorAlert(this.settingsProvider.getLabel("Please enter category"));
        }
        else if (self.faqsData.type == "" || self.faqsData.type.trim() == "") {
            this.errorAlert(this.settingsProvider.getLabel("Please enter type"));
        }
        else {
            if (this.new) {
                return new Promise(function (resolve, reject) {
                    self.settingsProvider.addFaqs(JSON.parse(JSON.stringify(self.faqsData)))
                        .then(function (result) {
                        console.log("add data", result);
                        if (result.status) {
                            self.faqsData.title = "";
                            self.faqsData.description = "";
                            var toast = self.toastCtrl.create({
                                message: self.settingsProvider.getLabel(result.message),
                                duration: 3000,
                                position: 'right',
                                cssClass: "success"
                            });
                            toast.present();
                            self.viewCtrl.dismiss();
                            resolve();
                        }
                    });
                });
            }
            else {
                console.log("selffaqsData", self.faqsData);
                return new Promise(function (resolve, reject) {
                    var _this = this;
                    self.settingsProvider.updateFaqs(JSON.parse(JSON.stringify(self.faqsData)))
                        .then(function (result) {
                        console.log("result", result);
                        if (result.status) {
                            self.faqsData.title = "";
                            self.faqsData.description = "";
                            var toast = self.toastCtrl.create({
                                message: self.settingsProvider.getLabel(result.message),
                                duration: 3000,
                                position: 'right',
                                cssClass: "success"
                            });
                            toast.present();
                            self.viewCtrl.dismiss();
                            resolve();
                        }
                        else {
                            _this.errorAlert(result.message);
                        }
                    });
                });
            }
        }
    };
    FaqsPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.settingsProvider.getLabel("Error"),
            message: message,
            buttons: [
                {
                    text: this.settingsProvider.getLabel("Ok"),
                    role: this.settingsProvider.getLabel('Ok'),
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return FaqsPage;
}());
FaqsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-faqs',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\faqs\faqs.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons start>\n\n    </ion-buttons>\n\n    <ion-title>{{settingsProvider.getLabel(title)}}</ion-title>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-grid class="table-cell">\n\n\n\n\n\n    <ion-row *ngIf="!new">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="addFaqs()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row *ngIf="new">\n\n      <ion-col>\n\n        <ion-toolbar>\n\n          <ion-buttons end>\n\n            <button ion-button small round icon-left color="primary" (click)="addFaqs()">\n\n              <ion-icon name="checkmark-circle"></ion-icon>\n\n              {{settingsProvider.getLabel(\'Save Changes\')}}\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-toolbar>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label>{{settingsProvider.getLabel("Title")}}</ion-label>\n\n            <ion-input style="font-size: 13px;" [(ngModel)]="faqsData.title" type="text"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label>{{settingsProvider.getLabel("Description")}}</ion-label>\n\n            <ion-textarea style="font-size: 13px;" [(ngModel)]="faqsData.description"></ion-textarea>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked>{{settingsProvider.getLabel("Category")}}</ion-label>\n\n            <!-- <ion-input  [(ngModel)]="faqsData.category" type="text"></ion-input> -->\n\n            <ion-select [(ngModel)]="faqsData.category" (ionChange)="setCategory() ">\n\n              <!-- <ion-option selected="true" (ionSelect)="selectCategory(null) ">Select Category</ion-option> -->\n\n              <ion-option *ngFor="let obj of categorylist" [selected]="faqsData.category==obj.categoryname" (ionSelect)="selectCategory(obj.categoryname) ">\n\n                {{obj.categoryname}}\n\n              </ion-option>\n\n            </ion-select>\n\n            <!-- <ion-select [(ngModel)]="faqsData.category" name="faqsData " multiple="false" style="font-size: 13px;">\n\n              <ion-option [selected]="faqsData  == \'\'" value="faqsData.category">Select Type</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Login Page\'">Login Page</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Accounts\'">Accounts</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Resolved by Customer\'">Resolved by Customer</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Saving Account\'">Saving Account</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Savings Account Trasactions\'">Savings Account Trasactions\n\n              </ion-option>\n\n              <ion-option [selected]="faqsData  == \'Savings Account Deposit Money\'">Savings Account Deposit Money\n\n              </ion-option>\n\n              <ion-option [selected]="faqsData  == \'Savings Account Send Money\'">Savings Account Send Money</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Transfer Between Savings\'">Transfer Between Savings</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Regular Savings Menu\'">Regular Savings Menu</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Savings Goals\'">Savings Goals</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Deposited Cheques List\'">Deposited Cheques List</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Mortgage Account\'">Not Valid</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Mortgage Account Transactions\'">Mortgage Account Transactions\n\n              </ion-option>\n\n              <ion-option [selected]="faqsData  == \'Mortgage Data\'">Mortgage Data</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Mortgage Calculator\'">Mortgage Calculator</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Overpayment Calculator\'">Overpayment Calculator</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Rate Interest Calculator\'">Rate Interest Calculator</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Pay Contacts\'">Pay Contacts</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Add Savings Account\'">Add Savings Account</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Savings- Analysis\'">Savings- Analysis</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Contact Us\'">Contact Us</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Third Party Notification\'">Third Party Notification</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Settings\'">Settings</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Saver Menu\'">Saver Menu</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Sweeper\'">Sweeper</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Saver\'">Saver</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Messages\'">Messages</ion-option>\n\n              <ion-option [selected]="faqsData  == \'Products\'">Products</ion-option>\n\n            </ion-select> -->\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label>{{settingsProvider.getLabel("Type")}}</ion-label>\n\n            <ion-input style="font-size: 13px;" [(ngModel)]="faqsData.type" type="text"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\faqs\faqs.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_masterpermission_masterpermission__["a" /* MasterpermissionProvider */]])
], FaqsPage);

//# sourceMappingURL=faqs.js.map

/***/ })

});
//# sourceMappingURL=24.js.map