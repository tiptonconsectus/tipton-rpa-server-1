webpackJsonp([15],{

/***/ 582:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsListPageModule", function() { return NewsListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__news_list__ = __webpack_require__(619);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NewsListPageModule = (function () {
    function NewsListPageModule() {
    }
    return NewsListPageModule;
}());
NewsListPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__news_list__["a" /* NewsListPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__news_list__["a" /* NewsListPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], NewsListPageModule);

//# sourceMappingURL=news-list.module.js.map

/***/ }),

/***/ 619:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_table_table__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_news_news__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NewsListPage = (function () {
    function NewsListPage(navCtrl, navParams, languageProvider, oauthProvider, toastCtrl, alertCtrl, viewCtrl, newsProvider, settingsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.newsProvider = newsProvider;
        this.settingsProvider = settingsProvider;
        this.newsOptions = {
            data: [],
            filteredData: [],
            columns: {
                "title": { title: "Title", search: true },
                "body": { title: "Body", search: true },
                "image": { title: "Image", format: 'function', fn: this.getPhoto.bind(this) },
                "action": { title: "Action Url", search: true },
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Delete",
                    action: this.deleteNews.bind(this),
                    color: "danger"
                },
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add Marketing News",
                addAction: this.createNews.bind(this),
            },
            tapRow: this.editNews.bind(this),
            uploadOptions: {},
        };
        this.languageString = "en";
        this.title = "News Management";
    }
    NewsListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewsListPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    NewsListPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        var self = this;
        this.newsProvider.getNews().then(function (data) {
            console.log("data", data);
            self.newsOptions.data = data;
            self.newsOptions = Object.assign({}, self.newsOptions);
        });
    };
    NewsListPage.prototype.getPhoto = function (image) {
        console.log("image", image);
        var asf = '<img class="imgUpload"  style="width:50%; height:50%" src ="' + image + '" alt="Click here to upload Image">';
        return asf;
    };
    NewsListPage.prototype.deleteNews = function (newsData) {
        var d = [];
        var self = this;
        for (var _i = 0, newsData_1 = newsData; _i < newsData_1.length; _i++) {
            var news1 = newsData_1[_i];
            d.push(this.newsProvider.deleteNews(news1));
        }
        Promise.all(d)
            .then(function () {
            var toast = self.toastCtrl.create({
                message: newsData.length == 1 ? self.settingsProvider.getLabel('News was Deleted successfully') : newsData.length + self.settingsProvider.getLabel(" News were Deleted successfully"),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            self.loadNewsData();
        });
    };
    NewsListPage.prototype.createNews = function () {
        this.navCtrl.push('NewsPage');
    };
    NewsListPage.prototype.editNews = function (news) {
        this.navCtrl.push('NewsPage', { news: news });
    };
    NewsListPage.prototype.loadNewsData = function () {
        var self = this;
        this.newsProvider.getNews()
            .then(function (data) {
            self.newsOptions.data = data;
            self.newsOptions = Object.assign({}, self.newsOptions);
        });
    };
    return NewsListPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1__components_table_table__["a" /* TableComponent */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__components_table_table__["a" /* TableComponent */])
], NewsListPage.prototype, "table", void 0);
NewsListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-news-list',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\news-list\news-list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{settingsProvider.getLabel(\'Marketing News Management | Consectus\')}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <table-component [options]="newsOptions"></table-component>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\news-list\news-list.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_news_news__["a" /* NewsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_settings_settings__["a" /* SettingsProvider */]])
], NewsListPage);

//# sourceMappingURL=news-list.js.map

/***/ })

});
//# sourceMappingURL=15.js.map