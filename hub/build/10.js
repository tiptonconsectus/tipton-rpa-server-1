webpackJsonp([10],{

/***/ 586:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RpaDashboardPageModule", function() { return RpaDashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rpa_dashboard__ = __webpack_require__(623);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RpaDashboardPageModule = (function () {
    function RpaDashboardPageModule() {
    }
    return RpaDashboardPageModule;
}());
RpaDashboardPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__rpa_dashboard__["a" /* RpaDashboardPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__rpa_dashboard__["a" /* RpaDashboardPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], RpaDashboardPageModule);

//# sourceMappingURL=rpa-dashboard.module.js.map

/***/ }),

/***/ 623:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RpaDashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_rpadashboard_rpa_dashboard__ = __webpack_require__(392);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RpaDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RpaDashboardPage = (function () {
    //upload path
    function RpaDashboardPage(navCtrl, navParams, oauthProvider, viewCtrl, settingsProvider, rpaProvider, toastCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.viewCtrl = viewCtrl;
        this.settingsProvider = settingsProvider;
        this.rpaProvider = rpaProvider;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.count = 45;
        this.currentTab = "Dashboard";
        this.prevAgent = null;
        this.enableAgentForm = false;
        this.timeout = 1;
        this.score = 0.90;
        this.actionPathOptions = {
            data: [],
            filteredData: [],
            columns: {
                "name": { title: "Action Path" },
                "execution_delay": { title: "Execution delay" },
                "AvgScore": { title: "AvgScore" },
            },
            pagination: false,
            fitler: null,
            search: false,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: false,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editNewAccount.bind(this),
            uploadOptions: {}
        };
        this.rpaList = {
            data: [],
            filteredData: [],
            columns: {
                "id": { title: "Agent Id", search: true },
                "agent_name": { title: "Agent Name", search: true },
                "agent_email": { title: "Agent Email", search: true },
                "host": { title: "Host Address", search: true },
                "port": { title: "Port", search: true },
                "settimeout": { title: "Timeout", search: true },
                "score": { title: "Score", search: true },
                "status": { title: "Status", format: 'function', fn: this.AgentListStatus.bind(this), search: true },
            },
            pagination: false,
            fitler: null,
            search: false,
            select: false,
            selectedActions: [],
            rowActions: [{ title: "update", action: this.UpdateAgent.bind(this), color: "primary" }, { title: "delete", action: this.DeleteAgent.bind(this), color: "orange" }],
            download: false,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: null,
            uploadOptions: {}
        };
        this.priority = {
            data: [],
            filteredData: [],
            columns: {
                "id": { title: "Action Id", search: true },
                "name": { title: "Action Name", search: true },
                "type": { title: "Action Type", search: true },
                "queued": { title: "Action Queued", search: true },
                "order_id": { title: "Priority", search: true },
            },
            pagination: false,
            fitler: null,
            search: false,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: false,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: this.editNewAccount.bind(this),
            uploadOptions: {}
        };
        // proirity
        //upload path
        this.actionpath = {
            data: [],
            filteredData: [],
            columns: {
                "id": { title: "Action Id", search: true },
                "version": { title: "Version", search: true },
                "name": { title: "Action Name", search: true },
                "type": { title: "Action Type", search: true },
                "queued": { title: "Action Queued", search: true },
                "created_on": { title: "Created On", search: true },
            },
            pagination: false,
            fitler: null,
            search: false,
            select: false,
            selectedActions: [],
            rowActions: [],
            download: false,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            resetFilter: null,
            add: {
                addActionTitle: "",
                addAction: "",
            },
            tapRow: null,
            uploadOptions: {}
        };
    }
    RpaDashboardPage.prototype.ionViewDidEnter = function () {
        this.settingsProvider.getAppLabels(this.viewCtrl.name);
        this.getAverageRpaData();
        this.getfindtotalagent();
        this.getAgentData();
        this.AllAgentList();
        this.GetActionPath();
        console.log("didenter");
    };
    RpaDashboardPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    RpaDashboardPage.prototype.editNewAccount = function () {
    };
    RpaDashboardPage.prototype.getColumns = function () {
        return ["agent_name", "status", "name", "host", "port", "count", "delay"];
    };
    RpaDashboardPage.prototype.getAverageRpaData = function () {
        var _this = this;
        var self = this;
        this.rpaProvider.getAverageRpa()
            .then(function (result) {
            _this.actionPathOptions.data = result.data;
            // this line reinitialized option again and assign it to table component
            _this.actionPathOptions = Object.assign({}, _this.actionPathOptions);
            console.log(result.data, "rpatype");
        }).catch(function (error) {
        });
    };
    RpaDashboardPage.prototype.getfindtotalagent = function () {
        var _this = this;
        var self = this;
        var agent_data = [];
        this.rpaProvider.getfindtotalagent()
            .then(function (result) {
            var previous;
            var arr_pos;
            var pos = 0;
            var common_agent = [];
            var actionPath_value = [];
            for (var i = 0; i < result.data.length; i++) {
                if (previous !== result.data[i].agent_name) {
                    actionPath_value = [];
                    arr_pos = pos;
                    console.log(arr_pos);
                    previous = result.data[i].agent_name;
                    common_agent.push({ "agent_name": result.data[i].agent_name, "status": result.data[i].status, "host": result.data[i].host, "port": result.data[i].port });
                    pos++;
                }
                actionPath_value.push({ "count": result.data[i].count, "delay": result.data[i].delay, "name": result.data[i].name });
                agent_data[arr_pos] = { "common_name": common_agent, "path_data": actionPath_value };
            }
            console.log("agent_data1", agent_data);
            _this.htmlContent = agent_data;
        }).catch(function (error) {
        });
    };
    // rpa agent management
    RpaDashboardPage.prototype.AgentListStatus = function (data) {
        var str;
        if (data == 1) {
            str = "Active";
        }
        else {
            str = "Disabled";
        }
        return str;
    };
    RpaDashboardPage.prototype.UpdateAgent = function (data) {
        // console.log(data);
        // return data
        this.navCtrl.push("UpdateagentPage");
    };
    RpaDashboardPage.prototype.DeleteAgent = function (Rpadata) {
        var _this = this;
        var self = this;
        var alert = this.alertCtrl.create({
            title: self.settingsProvider.getLabel("Confirm Delete"),
            message: self.settingsProvider.getLabel("Are you sure you want to delete Rpa Agent?"),
            buttons: [
                {
                    text: self.settingsProvider.getLabel("Cancel"),
                    role: self.settingsProvider.getLabel("cancel"),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: self.settingsProvider.getLabel("ok"),
                    handler: function (data) {
                        _this.rpaProvider.DeleteAgent(Rpadata)
                            .then(function (result) {
                            var toast = _this.toastCtrl.create({
                                message: _this.settingsProvider.getLabel("Rpa Agent Delete Successfully"),
                                duration: 3000,
                                position: 'right',
                                cssClass: 'success',
                            });
                            toast.present();
                            _this.getAgentData();
                        }).catch(function (error) {
                            // console.log("provider rpa error ",error.statusText)
                            var toast = _this.toastCtrl.create({
                                message: _this.settingsProvider.getLabel(error.statusText),
                                duration: 3000,
                                position: 'right',
                                cssClass: 'danger',
                            });
                            toast.present();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    RpaDashboardPage.prototype.pingAgent = function () {
        var _this = this;
        var data = {
            "ipaddress": this.ipaddress,
            "port": this.port
        };
        this.rpaProvider.pingAgent(data)
            .then(function (result) {
            var toast = _this.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Connection Established Successfully"),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            _this.enableAgentForm = true;
        }).catch(function (error) {
            console.log("provider rpa error ", error.statusText);
            var toast = _this.toastCtrl.create({
                message: _this.settingsProvider.getLabel(error.statusText),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
        });
    };
    RpaDashboardPage.prototype.saveAgent = function () {
        var _this = this;
        var more_name = [];
        var more_value = [];
        var data = {
            "host": this.ipaddress,
            "port": this.port,
            "agentname": this.name,
            "timeout": this.timeout,
            "adminemail": this.adminemail,
            "score": this.score,
            "agentnameadd": more_name,
            "agentvalueadd": more_value
            // url: "/admin/manage/save_entry",
            // data:{"host":hostaddress,"port":hostport,"agentname":agentname,"timeout":timeout,"adminemail":adminemail,"score":score,"agentnameadd":more_name,"agentvalueadd":more_value},
        };
        this.rpaProvider.SaveAgentdata(data)
            .then(function (result) {
            var toast = _this.toastCtrl.create({
                message: _this.settingsProvider.getLabel("Agent Added Successfully"),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            _this.enableAgentForm = false;
            _this.getAgentData();
        }).catch(function (error) {
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            var toast = _this.toastCtrl.create({
                message: _this.settingsProvider.getLabel(err.error_description),
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
        });
    };
    RpaDashboardPage.prototype.getAgentData = function () {
        var _this = this;
        var self = this;
        this.rpaProvider.getAgentData()
            .then(function (result) {
            _this.rpaList.data = result.agent;
            // this line reinitialized option again and assign it to table component
            _this.rpaList = Object.assign({}, _this.rpaList);
            console.log(result.agent, "rpadata list");
        }).catch(function (error) {
        });
    };
    // rpa agent management
    // proirity
    RpaDashboardPage.prototype.AllAgentList = function () {
        var _this = this;
        this.rpaProvider.AllAgentList()
            .then(function (result) {
            _this.agentList = result.agent;
            _this.priority.data = result.path;
            console.log(result.path, "result.path");
            _this.priority = Object.assign({}, _this.priority);
        }).catch(function (error) {
        });
    };
    // proirity
    //upload path
    RpaDashboardPage.prototype.GetActionPath = function () {
        var _this = this;
        this.rpaProvider.GetActionPath()
            .then(function (result) {
            // this.agentList = result.agent;
            _this.actionpath.data = result.path;
            console.log(result.path, "result.path");
            _this.actionpath = Object.assign({}, _this.actionpath);
        }).catch(function (error) {
        });
    };
    return RpaDashboardPage;
}());
RpaDashboardPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-rpa-dashboard',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\rpa-dashboard\rpa-dashboard.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>RPA DASHBOARD</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-segment [(ngModel)]="currentTab">\n\n    <!-- (ionChange)="segmentChanged($event)" -->\n\n    <ion-segment-button value="Dashboard">\n\n      Dashboard\n\n    </ion-segment-button>\n\n    <ion-segment-button value="UserManagement">\n\n      User Management\n\n    </ion-segment-button>\n\n    <ion-segment-button value="PermissionManagement">\n\n      Permission Management\n\n    </ion-segment-button>\n\n    <ion-segment-button value="ActionPathPriority">\n\n      Action Path Priority\n\n    </ion-segment-button>\n\n    <ion-segment-button value="ActionPathManagement">\n\n      Action Path Management\n\n    </ion-segment-button>\n\n    <ion-segment-button value="RPAAgentManagement">\n\n      RPA Agent Management\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n\n\n  <!-- Dashboard Section Starts here -->\n\n  <ion-grid *ngIf="currentTab == \'Dashboard\'">\n\n    <ion-row>\n\n\n\n      <ion-col col-3 style="margin : 0px;">\n\n        <ion-card class="ion-card-analytics user-card">\n\n          <ion-card-header text-center>\n\n            <div class="card-header">Users</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="people" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              <!-- <ion-icon name="arrow-dropright" class="right-icon"></ion-icon> -->\n\n              {{ count }}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n\n\n      <ion-col col-3>\n\n        <ion-card class="ion-card-analytics like-card">\n\n          <ion-card-header text-center>\n\n            <div class="card-header">Likes</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="thumbs-up" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              <!-- <ion-icon name="arrow-dropright" class="right-icon"></ion-icon> -->\n\n              {{ count }}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n\n\n      <ion-col col-3>\n\n        <ion-card class="ion-card-analytics uploads-card">\n\n          <ion-card-header text-center>\n\n            <div class="card-header">Uploads</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="copy" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              <!-- <ion-icon name="arrow-dropright" class="right-icon"></ion-icon> -->\n\n              {{ count }}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n\n\n      <ion-col col-3>\n\n        <ion-card class="ion-card-analytics stars-card">\n\n          <ion-card-header text-center>\n\n            <div class="card-header">Stars</div>\n\n          </ion-card-header>\n\n          <ion-card-content>\n\n            <div float-left>\n\n              <ion-icon name="star" class="ion-icon"></ion-icon>\n\n            </div>\n\n            <div float-right class="commonCount">\n\n              <!-- <ion-icon name="arrow-dropright" class="right-icon"></ion-icon> -->\n\n              {{ count }}\n\n            </div>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n\n\n      <ion-card>\n\n        <ion-card-header>\n\n          <h1 style="color : black; margin : 10px;">Queue Status</h1>\n\n          <hr>\n\n        </ion-card-header>\n\n\n\n        <ion-grid>\n\n          <table-component [options]="actionPathOptions"></table-component>\n\n        </ion-grid>\n\n      </ion-card>\n\n <!-- this for dynamic table -->\n\n      <ion-card>\n\n        <ion-grid>\n\n          <ion-row>\n\n             <ion-col col-12>\n\n              <ion-row>\n\n                  <!-- col-sm-12 col-xl-12 col-md-6 col-lg-6 -->\n\n                <ion-col col-sm-12 col-md-6 col-lg-3 col-xl-3 *ngFor="let data of htmlContent; let i = index" [ngClass]="{disabledagent:data.common_name[i].status==0}"> \n\n                  <ion-row *ngIf="data.common_name[i].status==0" class="disabled-agent-label">\n\n                      <span style="color:red; font-size:25px; margin-left:45%;"><b>Disabled</b></span>\n\n                  </ion-row>\n\n<!-- header -->\n\n                  <ion-row >\n\n                      <!-- str +="<div class=\'col-md-3 disabledagent\' >"\n\n                          str +="<div class=\'disabled-agent-label\'><span style=\'color:red; font-size:25px;\'><b>Disabled</b></span></div>" -->\n\n                    <ion-col col-12 class="agent-header">\n\n                      <span style="color: white;text-align: center;display: block;">{{data.common_name[i].host}}/{{data.common_name[i].port}}</span>\n\n                    </ion-col>\n\n                  </ion-row> \n\n                  <!-- header -->\n\n                  <!-- body -->\n\n                  <ion-row *ngFor="let value of data.path_data">\n\n                    <ion-col col-6 class="agent-name">\n\n                      <span>{{value.name}}</span>\n\n                    </ion-col>\n\n                \n\n                    <ion-col col-3 class="agent-count">\n\n                      <span>{{value.count}}</span>\n\n                    </ion-col>\n\n                \n\n                    <ion-col col-3 class="agent-avg">\n\n                      <span>{{value.delay}}</span>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                  <!-- body -->\n\n                </ion-col>\n\n              </ion-row>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n      </ion-card>\n\n      <!-- this for dynamic table -->\n\n    </ion-row>\n\n  </ion-grid>\n\n  <!-- Dashboard Section Ends here -->\n\n\n\n  <!-- User Management Sections Starts Here -->\n\n  <ion-grid *ngIf="currentTab == \'UserManagement\'">\n\n    <ion-card>\n\n\n\n      <div style="padding : 10px; width : 30%; float: left;">\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked style="font-size:20px !important;">User Name</ion-label>\n\n            <ion-input style="font-size:15px !important;" type="text" placeholder="Enter User Name" [(ngModel)]="Username"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked style="font-size:20px !important;">Email Address</ion-label>\n\n            <ion-input style="font-size:15px !important;" type="text" placeholder="Enter Email Address" [(ngModel)]="Email">\n\n            </ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked style="font-size:20px !important;">First Name</ion-label>\n\n            <ion-input style="font-size:15px !important;" type="text" placeholder="Enter First Name" [(ngModel)]="Firstname"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked style="font-size:20px !important;">Last Name</ion-label>\n\n            <ion-input style="font-size:15px !important;" type="text" placeholder="Enter Last Name" [(ngModel)]="Lastname"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked style="font-size:20px !important;">Password</ion-label>\n\n            <ion-input style="font-size:15px !important;" type="password" placeholder="Enter Password" [(ngModel)]="Password"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked style="font-size:20px !important;">Status</ion-label>\n\n            <ion-checkbox></ion-checkbox>\n\n            <!-- <ion-toggle color="secondary" [checked]="true"></ion-toggle> -->\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <hr>\n\n\n\n        <button ion-button color="blue">Submit</button>\n\n      </div>\n\n\n\n      <div style="float: left; width: 70%;">\n\n        <ion-card style="padding : 20px;">\n\n          <ion-label stacked style=" font-size:30px !important;">Permission List</ion-label>\n\n          <table-component></table-component>\n\n        </ion-card>\n\n      </div>\n\n\n\n    </ion-card>\n\n\n\n    <ion-card>\n\n      <table-component></table-component>\n\n    </ion-card>\n\n  </ion-grid>\n\n  <!-- User Management Sections Ends Here -->\n\n\n\n  <!-- Permission Management Sections Starts Here -->\n\n  <ion-grid *ngIf="currentTab == \'PermissionManagement\'">\n\n\n\n    <ion-card>\n\n      <div style="padding : 10px; width : 50%; float: left;">\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label stacked style="font-size:20px !important;">Create Feature</ion-label>\n\n            <ion-input style="font-size:15px !important;" type="text" placeholder="Enter Feature" [(ngModel)]="Feature">\n\n            </ion-input>\n\n            <br>\n\n\n\n            <p>This will Create New Feature For Permissions.</p>\n\n            <br>\n\n\n\n            <button ion-button color="blue">Submit</button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </div>\n\n    </ion-card>\n\n\n\n    <ion-card style="margin-top : 5px;">\n\n      <table-component></table-component>\n\n    </ion-card>\n\n\n\n  </ion-grid>\n\n  <!-- Permission Management Sections Ends Here -->\n\n\n\n  <!-- Action Path Priority Sections Starts Here -->\n\n  <ion-grid *ngIf="currentTab == \'ActionPathPriority\'">\n\n    <ion-card>\n\n      <ion-row>\n\n        <ion-col col-6>\n\n          <ion-item>\n\n            <ion-label stacked>Agent Name</ion-label>\n\n            <ion-select>\n\n              <ion-option selected value="select all agent">Select All Agent</ion-option>\n\n              <ion-option *ngFor="let agentdata of agentList" [value]="agentdata.id">{{agentdata.agent_name}}</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row>\n\n        <ion-col col-12>\n\n           <table-component  [options]="priority"></table-component>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n    </ion-card>\n\n  </ion-grid>\n\n  <!-- Action Path Priority Sections Ends Here -->\n\n\n\n  <!-- Action Path Management Sections Starts Here -->\n\n  <ion-grid *ngIf="currentTab == \'ActionPathManagement\'">\n\n\n\n    <ion-card>\n\n      <div style="padding : 10px; width : 100%; float: left;">\n\n        <ion-row>\n\n          <ion-col col-6>\n\n            <ion-label stacked >Name</ion-label>\n\n            <ion-input style="font-size:15px !important;" type="text" placeholder="Enter Name" [(ngModel)]="Name">\n\n            </ion-input>\n\n            <br>\n\n\n\n            <label style="border-radius:5px;"> Select File\n\n              <input type="file" id="File" accept="application/javascript" id="imageupload">\n\n              <!-- <ion-input type="file" accept=\'["application/zip", "application/octet-stream", "application/x-zip-compressed", "multipart/x-zip"]\' ></ion-input> -->\n\n              <!-- (change)="changeListener($event)" (input)="dirty()"> -->\n\n            </label>\n\n          </ion-col>\n\n\n\n          <ion-col col-6>\n\n            <ion-item>\n\n              <ion-label stacked>Type</ion-label>\n\n              <ion-select [(ngModel)]="type">\n\n                <ion-option selected value="nodejs">nodejs</ion-option>\n\n                <ion-option value="script">script</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n              <ion-label stacked >Queue</ion-label>\n\n              <ion-select [(ngModel)]="queue">\n\n                <ion-option selected value="true">true</ion-option>\n\n                <ion-option value="false">false</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-col>\n\n\n\n        </ion-row>\n\n      </div>\n\n    </ion-card>\n\n\n\n    <ion-card style="margin-top : 5px;">\n\n      <table-component [options]="actionpath"></table-component>\n\n    </ion-card>\n\n\n\n  </ion-grid>\n\n  <!-- Action Path Management Sections Ends Here -->\n\n\n\n  <!-- Action Path Management Sections Starts Here -->\n\n  <ion-grid *ngIf="currentTab == \'RPAAgentManagement\'">\n\n    <ion-card>\n\n      <ion-row style="padding : 10px;">\n\n        <ion-col col-4>\n\n          <ion-label stacked >IP Address</ion-label>\n\n          <ion-input style="font-size:15px !important;" type="text" placeholder="Enter IP Address" [(ngModel)]="ipaddress"></ion-input>\n\n        </ion-col>\n\n\n\n        <ion-col col-2>\n\n          <ion-label stacked >Port</ion-label>\n\n          <ion-input style="font-size:15px !important;" type="text" placeholder="Enter Port" [(ngModel)]="port">\n\n          </ion-input>\n\n        </ion-col>\n\n        <ion-col col-2>\n\n          <br>\n\n          <br>\n\n          <button ion-button Default round icon-left color="primary" (click)="pingAgent()">connect</button>\n\n          <!-- <button ion-button color="blue" shape="round" fill="outline" size="small">Connect</button> -->\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n    <ion-card *ngIf="enableAgentForm==true">\n\n          <ion-card-header>\n\n            <h1 style="color : black; margin : 10px;">Save Agent Setting</h1>\n\n            <hr>\n\n          </ion-card-header>\n\n        <ion-row style="padding : 10px;">\n\n          <ion-col col-4>\n\n              <ion-label stacked>Agent Name</ion-label>\n\n              <ion-input  type="text" placeholder="Enter Agent Name" [(ngModel)]="name">\n\n              </ion-input>\n\n          </ion-col>\n\n          <ion-col col-2>\n\n              <ion-label stacked>Set Sikuli TimeOut</ion-label>\n\n              <ion-input type="text" placeholder="Enter Set Sikuli TimeOut" [(ngModel)]="timeout">\n\n              </ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row style="padding : 10px;">\n\n            <ion-col col-4>\n\n                <ion-label stacked >Admin Email</ion-label>\n\n                <ion-input  type="text" placeholder="Enter Admin Email" [(ngModel)]="adminemail">\n\n                </ion-input>\n\n            </ion-col>\n\n            <ion-col col-2>\n\n                <ion-label stacked>Set Sikuli MatchScore</ion-label>\n\n                <ion-input type="text" placeholder="Enter Set Sikuli MatchScore" [(ngModel)]="score">\n\n                </ion-input>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col col-sm-12 col-md-12 col-lg-12>\n\n                <button ion-button Default round icon-left color="primary" (click)="saveAgent()">Save</button>\n\n            </ion-col>\n\n          </ion-row>\n\n    </ion-card>\n\n    <ion-card>\n\n      <div style="padding : 10px;">\n\n        <table-component [options]="rpaList"></table-component>\n\n        <!-- <table-component></table-component> -->\n\n      </div>\n\n    </ion-card>\n\n  </ion-grid>\n\n  <!-- Action Path Management Sections Ends Here -->\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\rpa-dashboard\rpa-dashboard.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_rpadashboard_rpa_dashboard__["a" /* RpaDashboardProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], RpaDashboardPage);

//# sourceMappingURL=rpa-dashboard.js.map

/***/ })

});
//# sourceMappingURL=10.js.map