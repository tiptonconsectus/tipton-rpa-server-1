webpackJsonp([21],{

/***/ 575:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguagesPageModule", function() { return LanguagesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__languages__ = __webpack_require__(612);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LanguagesPageModule = (function () {
    function LanguagesPageModule() {
    }
    return LanguagesPageModule;
}());
LanguagesPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__languages__["a" /* LanguagesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__languages__["a" /* LanguagesPage */]),
        ],
    })
], LanguagesPageModule);

//# sourceMappingURL=languages.module.js.map

/***/ }),

/***/ 612:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_language_language__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loadaudit_loadaudit__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LanguagesPage = (function () {
    function LanguagesPage(navCtrl, navParams, languageProvider, oauthProvider, alertCtrl, loadauditProvider, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.languageProvider = languageProvider;
        this.oauthProvider = oauthProvider;
        this.alertCtrl = alertCtrl;
        this.loadauditProvider = loadauditProvider;
        this.toastCtrl = toastCtrl;
        this.language = {
            name: "",
            translation: "",
            pagename: ""
        };
        this.title = "Languages";
    }
    LanguagesPage.prototype.ionViewDidLoad = function () {
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
        var self = this;
        var message = "Viewed " + this.language.name + " in Languages";
        var auditUser = {
            "users_id": this.oauthProvider.user[0].users_id,
            "customer_devices_id": 0,
            "usertype": "staff",
            "uuid": "",
            "appname": "hub",
            "event_type": "view",
            "screen": "languages",
            "trail_details": message
        };
        this.loadauditProvider.loadaudit(auditUser)
            .then(function (data) {
        });
    };
    LanguagesPage.prototype.addLanguageText = function () {
        var self = this;
        if (self.language.name.trim() == "") {
            self.errorAlert("Please enter lable name");
        }
        else if (self.language.translation.trim() == "") {
            self.errorAlert("Please enter translation label name");
        }
        else if (self.language.pagename.trim() == "") {
            self.errorAlert("Please select page name");
        }
    };
    LanguagesPage.prototype.errorAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: this.languageProvider.get("Error"),
            message: this.languageProvider.get(message),
            buttons: [
                {
                    text: this.languageProvider.get("Error"),
                    role: 'Ok',
                    handler: function (data) {
                    }
                }
            ]
        });
        alert.present();
    };
    return LanguagesPage;
}());
LanguagesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-languages',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\languages\languages.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n\n    <ion-title padding-vertical>{{languageProvider.get(\'Languages\')}} | Consectus</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-grid class="table-cell" padding>\n\n    <ion-row padding>\n\n      <ion-col col-6 padding>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label>{{languageProvider.get("Text Name")}}</ion-label>\n\n            <ion-input [(ngModel)]="language.name" type="text"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-label>{{languageProvider.get("Text Translation")}}</ion-label>\n\n            <ion-input [(ngModel)]="language.translation" type="text"></ion-input>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-item no-padding>\n\n              <ion-label stacked>{{languageProvider.get("Page Name")}}</ion-label>\n\n              <ion-select [(ngModel)]="language.pagename">\n\n                <ion-option value="">Select Page</ion-option>\n\n                <ion-option value="home">{{languageProvider.get("Home")}}</ion-option>\n\n                <ion-option value="setting">{{languageProvider.get("Setting")}}</ion-option>\n\n                <ion-option value="account">{{languageProvider.get("Account")}}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <button ion-button (click)="addLanguageText()">\n\n              <span>{{languageProvider.get("Submit")}}</span>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\languages\languages.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_language_language__["a" /* LanguageProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */]])
], LanguagesPage);

//# sourceMappingURL=languages.js.map

/***/ })

});
//# sourceMappingURL=21.js.map