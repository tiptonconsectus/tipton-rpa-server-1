webpackJsonp([25],{

/***/ 570:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqCategoryPageModule", function() { return FaqCategoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__faq_category__ = __webpack_require__(607);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FaqCategoryPageModule = (function () {
    function FaqCategoryPageModule() {
    }
    return FaqCategoryPageModule;
}());
FaqCategoryPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__faq_category__["a" /* FaqCategoryPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__faq_category__["a" /* FaqCategoryPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_table_table_module__["a" /* TableComponentModule */]
        ],
    })
], FaqCategoryPageModule);

//# sourceMappingURL=faq-category.module.js.map

/***/ }),

/***/ 607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqCategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_faq_category_faq_category__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the FaqCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FaqCategoryPage = (function () {
    function FaqCategoryPage(navCtrl, navParams, oauthProvider, faqCategoryProvider, toastCtrl, loadauditProvider, settingsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.oauthProvider = oauthProvider;
        this.faqCategoryProvider = faqCategoryProvider;
        this.toastCtrl = toastCtrl;
        this.loadauditProvider = loadauditProvider;
        this.settingsProvider = settingsProvider;
        this.faqCategory = {
            categoryname: ""
        };
        this.currentTab = "activeFaqCategory";
        this.faqCategoryOptions = {
            data: [],
            filteredData: [],
            columns: {
                "categoryname": { title: "Category", search: true, mobile: true, col: 6 },
                "created_date": { title: "Created Date", search: true, mobile: true, col: 2, format: 'function', fn: this.formatDateTime.bind(this) }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Archive",
                    action: this.delete.bind(this),
                    color: "danger"
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {
                addActionTitle: "Add FAQ Category",
                addAction: this.createFAQ.bind(this),
            },
            tapRow: this.edit.bind(this),
            uploadOptions: {},
        };
        this.faqCategory10Options = {
            data: [],
            filteredData: [],
            columns: {
                "categoryname": { title: "Category", search: true, mobile: true, col: 6 },
                "created_date": { title: "Created Date", search: true, mobile: true, col: 2, format: 'function', fn: this.formatDateTime.bind(this) }
            },
            pagination: true,
            fitler: null,
            search: true,
            select: true,
            selectedActions: [
                {
                    title: "Activate",
                    action: this.activate.bind(this),
                    color: 'primary'
                }
            ],
            rowActions: [],
            download: true,
            btnSize: "small",
            pageSize: 10,
            rowClass: "table-row",
            columnClass: "table-column",
            add: {},
            // tapRow: this.edit.bind(this),
            uploadOptions: {},
        };
    }
    FaqCategoryPage.prototype.edit = function (categories) {
        this.navCtrl.push("FaqCategoryDetailPage", { category: categories });
    };
    FaqCategoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FaqCategoryPage');
        if (!this.oauthProvider.user) {
            this.navCtrl.setRoot("LoginPage");
        }
    };
    FaqCategoryPage.prototype.ionViewDidEnter = function () {
        switch (this.currentTab) {
            case 'activeFaqCategory':
                this.getFaqCategory();
                break;
            case 'archiveFaqCategory':
                this.getArchiveCategory();
                break;
            default:
                break;
        }
    };
    FaqCategoryPage.prototype.createFAQ = function () {
        this.navCtrl.push("FaqCategoryDetailPage");
    };
    FaqCategoryPage.prototype.getFaqCategory = function () {
        var _this = this;
        var self = this;
        this.faqCategoryProvider.getallcategories()
            .then(function (result) {
            self.faqCategoryOptions.data = _this.archive(result.data, 1);
            self.faqCategoryOptions = Object.assign({}, self.faqCategoryOptions);
        }).catch(function (error) {
        });
    };
    FaqCategoryPage.prototype.getArchiveCategory = function () {
        var _this = this;
        var self = this;
        this.faqCategoryProvider.getallcategories()
            .then(function (result) {
            self.faqCategory10Options.data = _this.archive(result.data, 0);
            console.log("resultData", result.data);
            self.faqCategory10Options = Object.assign({}, self.faqCategory10Options);
            console.log(self.faqCategory10Options, "self.faqCategory10Options 123");
        }).catch(function (error) {
        });
    };
    FaqCategoryPage.prototype.activate = function (categoryId) {
        var _this = this;
        var d = [];
        var self = this;
        for (var _i = 0, categoryId_1 = categoryId; _i < categoryId_1.length; _i++) {
            var item = categoryId_1[_i];
            d.push(item.category_id);
        }
        var categorysId = d.join(',');
        self.faqCategoryProvider.activateFaqCategory(categorysId).then(function (result) {
            self.getArchiveCategory();
            var toast = self.toastCtrl.create({
                message: self.settingsProvider.getLabel(result.message),
                duration: 3000,
                position: 'right',
                cssClass: 'success',
            });
            toast.present();
            var message = "Faq are activated for this user in Faq Categories ";
            var auditUser = {
                "users_id": _this.oauthProvider.user[0].users_id,
                "customer_devices_id": 0,
                "usertype": "staff",
                "uuid": "",
                "appname": "hub",
                "event_type": "view",
                "screen": "push-messaging",
                "trail_details": message
            };
            _this.loadauditProvider.loadaudit(auditUser)
                .then(function (data) {
            });
        });
    };
    FaqCategoryPage.prototype.archive = function (archiveData, activestatus) {
        var archive = [];
        for (var i in archiveData) {
            if (archiveData[i].isactive == activestatus) {
                archive.push(archiveData[i]);
            }
        }
        return archive;
    };
    FaqCategoryPage.prototype.delete = function (items) {
        var _this = this;
        console.log("items", items);
        var d = [];
        var self = this;
        console.log("delete product");
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            d.push(item.category_id);
        }
        var categoryIds = d.join(',');
        self.faqCategoryProvider.deleteCategory(categoryIds).then(function (result) {
            // self.getArchiveCategory();
            var toast = self.toastCtrl.create({
                message: "Categories Archived Successfully",
                duration: 3000,
                position: 'right',
                cssClass: 'danger',
            });
            toast.present();
            _this.getFaqCategory();
        });
    };
    FaqCategoryPage.prototype.segmentChanged = function (event) {
        console.log("eventValue", event._value);
        switch (event._value) {
            case 'activeFaqCategory':
                this.getFaqCategory();
                break;
            case 'archiveFaqCategory':
                this.getArchiveCategory();
                break;
            default:
                break;
        }
    };
    FaqCategoryPage.prototype.formatDateTime = function (date) {
        // console.log(nullData, "nullData");
        var str = "";
        if (date !== null && date !== "") {
            var newdate = new Date(date);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1; //January is 0!
            var yyyy = newdate.getFullYear();
            // var hh = newdate.getHours();
            // var min = newdate.getMinutes().toString().length == 1 ? "0" + newdate.getMinutes().toString() : newdate.getMinutes();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            // newdate = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + min + ":00";
            newdate = dd + '/' + mm + '/' + yyyy;
            return (newdate);
        }
        else {
            str = '--';
            return (str);
        }
    };
    return FaqCategoryPage;
}());
FaqCategoryPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-faq-category',template:/*ion-inline-start:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\faq-category\faq-category.html"*/'<!--\n  Generated template for the FaqCategoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <img src="assets/images/logo-white(2).png" height="58" float-right>\n    <ion-title padding-vertical>Faq Category | Consectus</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-content padding>\n    <ion-segment [(ngModel)]="currentTab" (ionChange)="segmentChanged($event)">\n      <ion-segment-button value="activeFaqCategory">\n        Active\n      </ion-segment-button>\n      <ion-segment-button value="archiveFaqCategory">\n        Archive\n      </ion-segment-button>\n    </ion-segment>\n\n    <ion-grid class="table-cell" *ngIf="currentTab == \'activeFaqCategory\'">\n      <ion-row>\n        <ion-col>\n          <ion-grid class="table-simple">\n            <ion-row>\n              <ion-col>\n                <table-component [options]="faqCategoryOptions"></table-component>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid class="table-cell" *ngIf="currentTab == \'archiveFaqCategory\'">\n      <ion-row>\n        <ion-col>\n          <ion-grid class="table-simple">\n            <ion-row>\n              <ion-col>\n                <table-component [options]="faqCategory10Options"></table-component>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-content>'/*ion-inline-end:"C:\Users\consectus-backend\Desktop\HubConsectus\consectus-dashboard\src\pages\faq-category\faq-category.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_oauth_oauth__["a" /* OauthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_faq_category_faq_category__["a" /* FaqCategoryProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_loadaudit_loadaudit__["a" /* LoadauditProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__["a" /* SettingsProvider */]])
], FaqCategoryPage);

//# sourceMappingURL=faq-category.js.map

/***/ })

});
//# sourceMappingURL=25.js.map