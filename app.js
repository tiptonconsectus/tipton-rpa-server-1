const express = require('express');
const compression = require('compression');
var cors = require('cors');
const config = require('./config/config');
const path = require('path');
const cookieParser = require('cookie-parser');
let multer = require('multer');
const os = require('os');
const OauthToken = require('./services/oauth-rpa/oauthRPA');
var logger = require('./services/logger/log');

const debug = require("debug")("consectus.app.js");
const debugerr = require("debug")("consectus.app.js Error");

const db = require("./services/dbhelper/sqlhelper");
const migration = require('./migration/migration_db');

const app = express();

app.oauthTokens = {};
app.getOauthToken = function (host, port, https = false) {
  if (host !== undefined && port !== undefined) {
    const key = host + ":" + port;
    if (app.oauthTokens[key] !== undefined) {
      return app.oauthTokens[key];
    } else {
      app.oauthTokens[key] = new OauthToken(host, port, 5000, https);
      return app.oauthTokens[key];
    }
  }
}

app.use(cookieParser());

app.use("/", express.static(__dirname + "/www"));
app.set('view engine', 'pug');
app.set('views', './resources');
app.use("/static", express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, "www")));

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json({ limit: '10mb' }); //{
const urlEncoded = bodyParser.urlencoded({ limit: '10mb', extended: true }); //


// function log(req, res, next) {
//     logger.info("status-" + res.statusCode + " : " + req.method + " : " + req.url);
//     next();
// }


const Port = config.port;
// all environments
app.set('port', process.env.PORT || process.argv[2] || Port);
app.set('superSecret', config.secret);
app.disable("x-powered-by");

app.use(cors());
app.use(jsonParser);
app.use(urlEncoded);

app.use(compression());
// app.use(log);
app.executeQuery = db.executeQuery
const options = {
  app: app,
  port: 9090,

}
app.attempt = 1;
app.oauth2 = require('./services/oauth2');
app.permission = require('./middleware/permission');
app.jsonrequest = require("./middleware/checkjson");

app.all("/*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST,DELETE")
  res.header("Access-Control-Allow-Headers", "Cache-Control,Pragma, Origin, Authorization, Content-Type, X-Requested-With,X-XSRF-TOKEN, query,x-access-token")
  next()
})



migration.initialize(db.executeQuery);



var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, os.tmpdir());
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
app.upload = multer({ storage: storage });

app.oauth2.oauth2app(options);

/// routes file///


const login = require('./route/login_r')(app)
app.use('/', login)

const dash = require('./route/dashboard_r')(app)
app.use('/admin', dash)

var Est = require('./route/est_r')(app);
app.use('/estimated', Est);

const manage = require('./route/management_r')(app)
app.use('/admin/management', manage)

const rpa = require('./route/rpa_r')(app)
app.use('/admin/manage', rpa)

const action = require('./route/action_path_r.js')(app)
app.use('/admin/manage/action-path', action)

//const customer = require('./route/customer_r.js')(app)
//app.use('/customer', customer)

const queue = require('./route/queue_r.js')(app)
app.use('/updatequeue', queue)

const task = require('./route/task_r.js')(app)
app.use('/tasks', task)

const orderpath = require('./route/action-path-order_r.js')(app)
app.use('/admin/manage/order', orderpath)

const sopraAccNo = require('./route/sopra_accno_r')(app)
app.use('/admin/manage/sopra', sopraAccNo)

app.get('/login', (req, res) => {
  res.render('login/login');
});


// app.get('/next', app.oauth2.authorize, app.permission, (req, res) => {
//   res.render('dashboard/dashboard');
// });
// app.get('/next/user', app.oauth2.authorize, (req, res) => {
//   res.render('dashboard/dashboard');
// });
// app.get('/test', (req, res) => {
//   res.render('pug/index');
// });
app.get('/unauthorized', app.oauth2.authorize, app.permission, (req, res) => {
  res.render('unauthorized/unauthorized');
});

// app.use('*',function (req, res, next) {
// //  res.status(404).send("Not found");
//   // next(createError(404));
//   let error
// });

app.use(function (err, req, res, next) {
  debugerr(err)
  let header = app.jsonrequest(req, "err")
  if (err.status === 407) {
    if (header) {
      res.status(407).json({
        status: "error",
        error: "unauthorized",
        error_description: "url not unauthorized"
      }).end();
    }
    else {
      res.render('unauthorized/unauthorized');
    }
  } else if (err.status === 404) {
    if (header) {
      res.status(404).json({
        status: "error",
        error: "unauthorized",
        error_description: "url not unauthorized"
      }).end();
    }
    else {
      res.render('unauthorized/unauthorized');
    }
  } else if (err.status === 404) {
    if (header) {
      res.status(404).json({
        status: "error",
        error: "url not found",
        error_description: "url not found"
      }).end();
    }
    else {

      res.render('notfound/404');
    }
  } else if (err.status === 401) {
    if (header) {
      res.status(401).json({
        status: "error",
        error: "url not found",
        error_description: "url not found"
      }).end();
    } else {
      res.render('notfound/404');
    }

  } else {
    next(err);
  }
})


app.listen(Port, function () {
  debug('RPA Server is listing on port ' + Port + ' !')
  logger.info('RPA Server is listening on port ' + Port + ' !');
})
// }).catch((error)=>{
//   debug("Oauth2 App initialization failed:", error);
// })

const MonitorProcess = require("./action-paths/process");
const monitorProcess = new MonitorProcess(app.getOauthToken);
monitorProcess.start();