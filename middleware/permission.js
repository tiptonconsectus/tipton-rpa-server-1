
module.exports=async function permission(req,res,next){
    let verify=req.token;
    let url_path =(req.baseUrl==='') ? req.path : req.originalUrl;
    let header = req.app.jsonrequest(req,"permi")
    if(verify!==null && verify.Accesstoken!==undefined && verify.client!==undefined && verify.user!==undefined && verify.permissions!==undefined ){  
        res.locals.userfirst = verify.user.firstname;
        res.locals.userlast = verify.user.lastname;
        res.locals.client = verify.client.name;
        res.locals.clientid = verify.client.client_id;
        res.locals.clientsec = verify.client.client_secret;
       

        const perm=verify.permissions;
        let  check = await perm.filter((list) =>{
                if(list.url === "/*"){
                    return true;
                }else if(url_path.indexOf(list.url) >= 0){
                    return true;
                }
                else{                    
                    return false
                }
         });

        if(check.length>0){
              next()
         }else{
            if (url_path.indexOf("/oauth2") < 0) {
                if (header) {
                    res.status(401).json({
                        status: "error",
                        error: "invalid_permission",
                        error_description: "Invalid Permission"
                    }).end();
                }
                else{
                    let err = new Error('You are not Authorized');
                    err.status=407
                    next(err)
                }
               
            } else {
                res.status(407).json({
                    status: "error",
                    error: "Authorization Failed",
                    error_description: "You are Not Authorized"
                }).end();
    
            }
         }

    }else if(verify!==null && verify.user_id!==undefined && verify.user_id=== null){
        req.jsonApiRequest = true
      
        next();
    }else{
        if (url_path.indexOf("/oauth2") < 0) {
            if (header) {
                res.status(401).json({
                    status: "error",
                    error: "invalid_token",
                    error_description: "Invalid token"
                }).end();
              } else {
                res.redirect('/login');
              }
            
        } else {
            res.status(401).json({
                status: "error",
                error: "invalid_token",
                error_description: "Invalid token"
            }).end();

        }
    }
}


