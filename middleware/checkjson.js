module.exports=function checkJson(req,rex=null){
    
    let findjson;
     if(req.headers["accept"] !==undefined && req.headers.accept.indexOf('application/json') > -1){
         findjson =  true;
       
     }else if (req.rawHeaders !== undefined && req.rawHeaders.indexOf('application/json')>-1  ){
       
        findjson = true
     }else if (req.headers['content-type'] !== undefined && req.headers['content-type'].indexOf('application/json') > -1 ){
         findjson =  true;
       
     }else if (req.jsonApiRequest !==undefined){
        findjson =  req.jsonApiRequest
 
    }else if (req.xhr){
        findjson = req.xhr
    }else {
        findjson = false
    }
  
     return findjson;
}       