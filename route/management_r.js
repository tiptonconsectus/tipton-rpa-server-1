
let express = require('express');
let router = express.Router();
const config = require('../config/config');

module.exports = (app) => {

    let managements = require("../controller/management_c");
    let manage = new managements(app.oauth2);
  
    router.get('/user',app.oauth2.authorize, app.permission, function (req, res) {
       // login.Login(req, res);
       manage.getUserManagements(req,res)
      
    });

    router.post('/user',app.oauth2.authorize, app.permission,  function (req, res) {
        manage.userManagements(req,res)
     });
     router.post('/user/update/:id',app.oauth2.authorize, app.permission,  function (req, res) {
        manage.updateUserManagements(req,res)
     });
     router.get('/user/delete/:id',app.oauth2.authorize, app.permission,  function (req, res) {
        // console.log(req.params)
        manage.deleteUserManagements(req,res)
     });

    router.get('/permission',app.oauth2.authorize,app.permission,   function (req, res) {
        // login.Login(req, res);
        manage.urlManagements(req,res)
     //   res.render('dashboard/user/permission')
     });

     router.post('/permission', app.oauth2.authorize,app.permission,  function (req, res) {
        // login.Login(req, res);
        manage.createPermission(req,res)
     });
     router.post('/permission/update/:id', app.oauth2.authorize,app.permission,  function (req, res) {
        // login.Login(req, res);
        manage.updateUrlPermission(req,res)
     });

   //  router.get('/action-path',app.oauth2.authorize,app.permission,   function (req, res) {
   //      // login.Login(req, res);
   //      res.render('dashboard/action-path/actionpath')
   //  });

   //  router.get('/rpa-agent',app.oauth2.authorize, app.permission, function (req, res) {
   //      // login.Login(req, res);
   //      res.render('dashboard/RPA/rpa')
   //  });
    
    // router.get('/dashboard',app.oauth2.authorize, app.role, function (req, res) {
    //     // login.Login(req, res);
    //     res.render('dashboard/dashboard')
    //  });
   

    return router;
}