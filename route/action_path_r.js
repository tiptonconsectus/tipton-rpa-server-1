
let express = require('express');
let router = express.Router();
const config = require('../config/config');

module.exports = (app) => {

    let actionpath = require("../controller/action_path_c");
    let action = new actionpath();
  
    router.get('/',app.oauth2.authorize, app.permission, function (req, res) {
      
       action.GetActionPath(req,res);
      
    });
    router.post('/',app.oauth2.authorize, app.permission, app.upload.single('file'), function (req, res) {
      
        action.UploadActionPath(req,res);
       
     });
     router.get('/delete/:id/:name',app.oauth2.authorize, app.permission, function (req, res) {
      
       action.DeleteActionPath(req,res);
      
    });
   

    return router;
}