var express = require('express');
var router = express.Router();
const Estimated = require("../controller/est_c");
const estimated = new Estimated();

/* GET users listing. */
module.exports = (app) => {

  router.post("/callback", app.oauth2.authorize, estimated.getETA.bind(estimated));
  router.post("/tasks/update-queue-status/:agentid/:queueid", app.oauth2.authorize, estimated.agentCallBack.bind(estimated));
  router.get("/download", estimated.downloadFile.bind(estimated))
  return router;
}

