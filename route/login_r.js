
let express = require('express');
let router = express.Router();

module.exports = (app) => {
    let loginhandler=app.oauth2
    let loginController = require("../controller/login_c");
    let login = new loginController(loginhandler);

    // router.post('/', function (req, res) {
    //     if (req.xhr || req.headers.accept.indexOf('json') > -1 || req.originalUrl.indexOf("/oauth2") > -1) {
    //         res.status(401).json({
    //           status: "error",
    //           error: "url not found",
    //           error_description: "url not found"
    //         }).end();
    //       } else {
    //         res.render('notfound/404');
    //       }
    // });
   
    router.post('/login', function (req, res) {
        login.Login(req, res);
    });
    router.get('/login/logout',app.oauth2.authorize,function (req, res) {
       
        login.logout(req, res);
    });

   

    return router;
}