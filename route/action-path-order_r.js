const express = require('express');
const router = express.Router();
const config = require('../config/config');

module.exports = (app) => {

    let actionpath_order = require("../controller/action-path-order_c");
    let actionorder = new actionpath_order();
  
    router.get('/',app.oauth2.authorize, app.permission, function (req, res) {
      
       actionorder.GetActionPathOrder(req,res);
      
    });
    router.post('/',app.oauth2.authorize, app.permission, function (req, res) {
      
        actionorder.ActionOrder(req,res);
       
     });
    router.get('/:id',app.oauth2.authorize, function (req, res) {
      
        actionorder.ActionOrderId(req,res);
       
     });
    //  router.get('/:id',app.oauth2.authorize, app.permission, function (req, res) {
      
    //     actionorder.ActionOrderId(req,res);
       
    //  });
   

    return router;
}