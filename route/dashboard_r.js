
const express = require('express');
const router = express.Router();
const dash_c = require("../controller/dashboard_c");
const dash = new dash_c();


module.exports = (app) => {

    router.get('/dashboard',app.oauth2.authorize, function (req, res) {
        dash.getDashboard(req, res);

    });
    router.get('/average',app.oauth2.authorize, function (req, res) {
        dash.getAverage(req, res);
    });
    router.get('/get-queue-status',app.oauth2.authorize, function (req, res) {
        dash.getQueueStatus(req, res);
    });

    router.get('/get-agent',app.oauth2.authorize,  function (req, res) {
        dash.getTotalAgent(req, res);
    });

    router.get('/get-agent-path-queues',app.oauth2.authorize, function (req, res) {
        dash.getAgentPathQueues(req, res);
    });
    router.get('/get-completed-task',app.oauth2.authorize, function (req, res) {
        dash.getAgentCompleted(req, res);
    });



    return router;
}