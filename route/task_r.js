
let express = require('express');
let router = express.Router();

//const config = require('../config/config');

module.exports = (app) => {
    let actionpath = require("../controller/task_c");
    let action = new actionpath();
    router.post("/execute-action-path", app.oauth2.authorize, function (req, res) {
        action.ExecuteActionPath(req, res)

    });
    /** this url is used for callback*/
    router.post("/update-queue-status/:agentid/:queueid", app.oauth2.authorize, function (req, res) {
        action.UpdateQueueStatus(req, res)
    });


    router.post("/get-action-path-queue", app.oauth2.authorize, function (req, res) {
        action.GetActionPathQueue(req, res)
    });
    router.post("/get-action-path-queue-status", app.oauth2.authorize, function (req, res) {
        action.GetQueueStatus(req, res)
    });
    router.post("/get-pending-queue", app.oauth2.authorize, function (req, res) {
        action.GetPendingQueue(req, res)
    });

    // });
    /** this url is used for callback*/
    // router.post("/update-queue-status/:agentid/:queueid", custom_auth, function (req, res) {
    //     action.UpdateQueueStatus(req, res)
    // });
    // router.post("/get-action-path-queue", function (req, res) {
    //     action.GetActionPathQueue(req, res)
    // });
    // router.post("/get-action-path-queue-status", function (req, res) {
    //     action.GetQueueStatus(req, res)
    // });
    // router.post("/get-pending-queue", function (req, res) {
    //     action.GetPendingQueue(req, res)
    // });
    return router;
}