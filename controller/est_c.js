const EstimateTime_M = require("../models/est_m");
const fs = require("fs"); const path = require("path");
module.exports = class EstimateTime {
  constructor() {
    this.EstimateTime_m = new EstimateTime_M();
  }
  async getETA(req, res) {
    let queueid = parseInt(req.body.queueid)
    if (queueid) {
      this.EstimateTime_m.getTime(queueid).then((result) => {

        if (result.length !== 0) {
          if (result[0].agent_id) {
            let eta;
            try {
              if (result[0].name.indexOf('existing') >= 0) {
                eta = (result[0].position - 1) * 5;
              } else {
                eta = (result[0].position - 1) * 9;
              }
            } catch (e) {
              eta = (result[0].position - 1) * 9;
            }
            if (eta > 0) {
              res.status(200).json({
                status: true,
                message: "estimated time or callback step",
                data: { eta: eta, step: null }
              }).end();
            } else {
              this.EstimateTime_m.getStep(queueid).then((stepData) => {
                res.status(200).json({
                  status: true,
                  message: "estimated time or callback step",
                  data: { eta: null, step: stepData }
                }).end();
              }).catch((err) => {
                res.status(400).json({
                  status: false,
                  error: "invalid_request",
                  error_description: err
                }).end();
              })
            }

          } else {
            res.status(200).json({
              status: true,
              message: "estimated time or callback step",
              data: { eta: null, step: null }
            }).end();
          }

        } else {
          res.status(400).json({
            status: true,
            message: "completed",
            error_description: " requested queue has done there task"
          }).end();
        }

      }).catch((err) => {
        res.status(400).json({
          status: false,
          error: "invalid_request",
          error_description: err
        }).end();
      })
    } else {
      res.status(400).json({
        status: false,
        error: "invalid_request",
        error_description: "queueid not found"
      }).end();
    }

  }
  agentCallBack(req, res) {
    let queueid = parseInt(req.params.queueid);

    let step = req.body.step
    if (queueid && step) {
      this.EstimateTime_m.createStep(step, queueid).then((result) => {
        res.status(200).json({
          status: true,
          message: "estimated time or callback step",
          data: result
        }).end();
      }).catch((err) => {
        res.status(400).json({
          status: false,
          error: "invalid_request",
          error_description: err
        }).end();
      })
    } else {
      res.status(400).json({
        status: false,
        error: "invalid_request",
        error_description: "queueid and step  not found"
      }).end();
    }

  }

  downloadFile(req, res) {
    try {
      let filename = req.query.filename ? req.query.filename : "output.js";
      let foldername = req.query.foldername ? req.query.foldername : "temp";
      let file = path.join(__dirname, "../../" + foldername, filename);
      console.log("file", file)
      if (fs.existsSync(file)) {
        console.log("finded")
        res.download(file);
      } else {
        res.status(400).json({
          status: false,
          error: "invalid_request",
          error_description: "file not found"
        }).end();
      }
    } catch (err) {
      console.log("errrrrr", err)
    }

  }

}
