class Managements {
    constructor(management=null) {
       
        this.manage=management       
    }
    getUserManagements(req,res){
    
       // let formdata=req.errobj
        Promise.all([this.getUser(req,res), this.geturl(req,res)]).then(function(result) {
            if(req.errobj!==undefined){
         
                let data={client:req.token.client.client_id,client_secret:req.token.client.client_secret,user:result[0],urllist:result[1],
                    firstname:req.errobj.firstname,lastname:req.errobj.lastname,email:req.errobj.email,username:req.errobj.username}
                    if(req.updatecheck!==undefined){
                        data.savedata=req.updatecheck
                    }else{
                      data.savedata="save"
                    }
               
                res.render('dashboard/user/usermanagement',data);

            }else{
               res.render('dashboard/user/usermanagement',{client:req.token.client.client_id,client_secret:req.token.client.client_secret,user:result[0],urllist:result[1],savedata:"save"});
            }
        });
       
    }
    userManagements(req, res) {
      //  console.log(req.params)
    try{
        this.manage.createuserlogin(req,res,(err,result)=>{
      
            if(err){
               req.errobj=req.body
                req.body={}
                res.cookie('errormessage',err, {maxAge:9200});

                this.getUserManagements(req,res)

              }else{
                  //if(result.insertId)
                  req.body.userid=result.insertId

                  req.body.role=req.body.permarray

                  try{

                  this.manage.userrole(req,res,function(error,role){
                  
                   if(error){
                       res.cookie('message',"successfully created and role "+error, {maxAge:9200}).redirect('/admin/management/user');
                   }else{
                       res.cookie('message',"successfully created and role has been added", {maxAge:9200}).redirect('/admin/management/user');
                   }

               })
            }catch(err){
                throw error;
            }
              

            }
       })

    }catch(error){
        throw error;
    }
      

    }
    updateUserManagements(req,res){
        req.body.status=(req.body.status=="true")?1:0;
        try{
        this.manage.userupdate(req,res,(err,result)=>{
            if(err){
                req.updatecheck=req.params.id;
                req.errobj=req.body;
                req.body={};
                req.params={};
                res.cookie('errormessage',err, {maxAge:9200});

                this.getUserManagements(req,res)
               
             //  res.cookie('errormessage',err, {maxAge:9200}).redirect('/admin/management/user');

              }else{
               // res.cookie('message',"successfully Updated", {maxAge:7200}).redirect('/admin/management/user')
                 req.body.userid=req.params.id
                 req.body.role=req.body.permarray
                try{
                    this.manage.UpdateUserRole(req,res,function(error,result){
                            if(error){
                                res.cookie('errormessage',err, {maxAge:9200}).redirect('/admin/management/user');
                            }
                            else{
                                res.cookie('message',"successfully updated and role has been updated", {maxAge:9200}).redirect('/admin/management/user');
                            }
                    })
                }catch(err){
                    throw err;
                }
              

            }
       })
    }catch(err){
       throw err
    }
    }
    deleteUserManagements(req,res){
        req.body.status=3;
    
        try{
            this.manage.userupdate(req,res,function(err,result){
            
                if(err){
                   
                    res.status(400).json({
                        status: "error",
                        error: "invalid_request",
                        error_description: err
                    }).end();
    
                  }else{
                   res.status(200).json({
                        status: "success",
                        data: "deleted"
                    }).end();
                }
           })
        }catch(err){
              res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: err
            }).end();
        }
      
    }
    urlManagements(req,res){
        Promise.all([this.geturl(req,res)]).then(function(result) {
            res.render('dashboard/user/permission',{client:req.token.client.client_id,client_secret:req.token.client.client_secret,url:result[0]});
          });
     //   res.render('dashboard/user/permission')
    }
    createPermission(req, res) {
     
        this.manage.createurl(req,res,function(err,result){
             if(err){
                
                res.cookie('errormessage',err, {maxAge:9200}).redirect('/admin/management/permission');

               }else{
                res.cookie('message',"successfully created", {maxAge:9200}).redirect('/admin/management/permission');

             }
        })

    }
    updateUrlPermission(req,res){
     
                this.manage.updateurllist(req,res,function(err,result){
                    if(err){
                
                        res.cookie('errormessage',err, {maxAge:9200}).redirect('/admin/management/permission');
        
                       }else{
                        res.cookie('message',"successfully created", {maxAge:9200}).redirect('/admin/management/permission');
        
                     }

                })
    }
    getUser(req,res){
      return   new Promise((resolve, reject) => {
                    this.manage.UserPermissionList(req,res,(err,results)=>{
                        if(err){
                            reject(err);
                        }else{
                            resolve(results);
                        }
                    })
                })
       

    }
    geturl(req,res){
        return   new Promise((resolve, reject) => {
                    this.manage.urllist(req,res,function(err,results){
                        if(err){
                            reject(err);
                        }else{
                            resolve(results);
                        }
                    })
                })

    }

}
module.exports = Managements;