const parse = require('csv-parse');
const fs = require('fs');
const path = require('path');
const customerbank = path.join(__dirname, "../demofile/Customers.csv");
const CustomerSavingsAccounts = path.join(__dirname, "../demofile/CustomerSavingsAccounts.csv");
const SavingsAccountTransactions = path.join(__dirname, "../demofile/SavingsAccountTransactions.csv");
const morgage = path.join(__dirname, "../demofile/MortgagesAccounts.csv");
const morgagetrans = path.join(__dirname, "../demofile/Mortgagestrans.csv");

const debug = require("debug")("consectus.file:");
const config = require("../config/config");
const Query2 = require("../services/dbhelper/sqlhelper").Query2;
const mysql = require("mysql");
class customer {
    constructor(custbnk = customerbank, custsav = CustomerSavingsAccounts, trans = SavingsAccountTransactions, morg = morgage, morgtran = morgagetrans) {
        // this.file1 = fs.createReadStream(custbnk);
        this.file2 = fs.createReadStream(custsav);
        // this.file3 = fs.createReadStream(trans);
        // this.file4 = fs.createReadStream(morgage);
        // this.file5 = fs.createReadStream(morgagetrans);
        this.options = { delimiter: ",", cast: true, columns: true, ltrim: true, rtrim: true };


    }
    async ExecuteAll(req = null, res = null) {
        await this.CustomerBank_csv();
        // await this.CustomerAcc_csv();
        //    await this.CustomerTrans_csv();
        //    await this.Morg_csv();
        //    await this.MorgTrans_csv();


    }
    CustomerBank_csv(file = null) {
        const output = [];
        // const options = { delimiter: ",", cast: true, columns: true, ltrim: true, rtrim: true};

        this.file1
            .pipe(parse(this.options))
            .on('data', async (csvrow) => {
                console.log(csvrow);
                let result = await this.Bank_Find_m(csvrow)
                if (result.length === 0) {
                    await this.Bank_m(csvrow)
                }


            })
            .on('end', function (err) {
                //do something wiht csvData
                console.log(err);
            });



    }
    CustomerAcc_csv(file = null) {
        let check_acc;

        this.file2
            .pipe(parse(this.options))
            .on('data', async (csvrow) => {

                console.log(csvrow);
                debug(csvrow);
                check_acc = await this.CustSav_Find_m(csvrow.bankaccounts_id);

                if (check_acc.length === 0) {
                    let result = await this.Acc_typeFind(csvrow.account_type, csvrow.account_type_code);

                    if (result.length !== 0) {
                        await this.CustSav_m(result[0].id, csvrow)
                    } else {
                        let insert = await this.Acc_typeCreate(csvrow.account_type, csvrow.account_type_code)
                        await this.CustSav_m(insert.insertId, csvrow)
                    }
                }


            })
            .on('end', function () {
                //do something wiht csvData
                console.log(csvData);
            });



    }
    CustomerTrans_csv(file = null) {
        let check_acc;

        this.file2
            .pipe(parse(this.options))
            .on('data', async (csvrow) => {

                console.log(csvrow);
                debug(csvrow);
                check_acc = await this.CustTransFind_m(csvrow.bank_transaction_id);

                if (check_acc.length === 0) {
                    let result = await this.Acc_typeFind(csvrow.accounttype, csvrow.account_type_code);

                    if (result.length !== 0) {
                        await this.CustTrans_m(result[0].id, csvrow)
                    } else {
                        let insert = await this.Acc_typeCreate(csvrow.accounttype, csvrow.account_type_code)
                        await this.CustTrans_m(insert.insertId, csvrow)
                    }
                }


            })
            .on('end', function () {
                //do something wiht csvData
                console.log(csvData);
            });

    }
    Morg_csv(file = null) {
        let check_acc;

        this.file4
            .pipe(parse(this.options))
            .on('data', async (csvrow) => {

                console.log(csvrow);
                debug(csvrow);
                check_acc = await this.MorgFind_m(csvrow.bankaccounts_id);

                if (check_acc.length === 0) {

                    await this.Morg_m(csvrow)

                }


            })
            .on('end', function () {
                //do something wiht csvData
                console.log(csvData);
            });

    }

    MorgTrans_csv(file = null) {
        let check_acc;

        this.file5
            .pipe(parse(this.options))
            .on('data', async (csvrow) => {

                console.log(csvrow);
                debug(csvrow);
                check_acc = await this.MorgTransFind_m(csvrow.bank_transaction_id);

                if (check_acc.length === 0) {
                    let result = await this.Acc_typeFind(csvrow.accounttype, csvrow.account_type_code);

                    if (result.length !== 0) {
                        await this.MorgTrans_m(result[0].id, csvrow)
                    } else {
                        let insert = await this.Acc_typeCreate(csvrow.accounttype, csvrow.account_type_code)
                        await this.MorgTrans_m(insert.insertId, csvrow)
                    }
                }


            })
            .on('end', function () {
                //do something wiht csvData
                console.log(csvData);
            });

    }



    /** model start from here*/
    Bank_Find_m(data) {
        let query = "SELECT * FROM ?? WHERE ??=?"
        let inserts = [config.tables.bankcustomer, 'bankcustomers_id', data.bankcustomers_id];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })
    }
    Bank_m(data) {
        let date = new Date(data.dob.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        let query = "INSERT INTO ?? (??,??,??,??,??,??,??,??,??,??,??,??,??,??,??)"
        query += "  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        let inserts = [config.tables.bankcustomer,
            'bankcustomers_id', 'memberid', 'title', 'firstname', 'lastname', 'dob', 'mobileno_user', 'emailid_user', 'addr_number', 'addr_address_1',
            'addr_address_2', 'addr_address_3', 'addr_address_4', 'addr_address_5', 'postalcode',
        data.bankcustomers_id,
        data.customerid,
        data.title,
        data.firstname,
        data.lastname,
            date,
        data.mobileno,
        data.emailid,
        data.housenumber,
        data.address1,
        data.address2,
        data.address3,
        data.address4,
        data.address5,
        data.postcod];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }

    Acc_typeFind(type, code) {
        let query = "SELECT * FROM ?? WHERE ??=? AND ??=?"
        let inserts = [config.tables.acc, 'accountname', type, 'accountcode', code];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }
    Acc_typeCreate(type, code) {
        let query = "INSERT INTO ?? (??,??) VALUES(?,?)"
        let inserts = [config.tables.acc, 'accountname', 'accountcode', type, code];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }


    CustSav_m(acc_type_id, data) {
        let query = "INSERT INTO ?? (??,??,??,??,??,??,??,??,??,??,??,??)"
        query += "  VALUES(?,?,?,?,?,?,?,?,?,?,?,?)"
        let inserts = [config.tables.customersaving,
            'customers_id',
            'bankcustomers_id',
            'account_types_id',
            'accountno',
            'accountholdername',
            'available',
            'interestrate',
            'balance',
            'create_date',
            'modified_date',
            'acc_act_code',
            'tidecurrentaccount_sortcode',
        data.bankaccounts_id,
        data.bankcustomers_id,
            acc_type_id,
        data.accountno,
        data.accountholdername,
        data.available_balance,
        data.interestrate,
        data.balance,
        data.account_creation_date,
        data.lastupdated,
        data.tidecurrentaccount,
        data.tidecurrentaccountsortcode];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }

    CustSav_Find_m(bankaccounts_id) {
        let query = "SELECT * FROM ?? WHERE ??=?"
        let inserts = [config.tables.customersaving, 'customers_id', bankaccounts_id];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }
    CustTransFind_m(bank_transaction_id) {
        let query = "SELECT * FROM ?? WHERE ??=?"
        let inserts = [config.tables.customertranscation, 'customer_account_transactions_id', bank_transaction_id];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }
    CustTrans_m(acc_typeid, data) {
        let query = "INSERT INTO ?? (??,??,??,??,??,??,??,??,??,??,??,??)"
        query += "  VALUES(?,?,?,?,?,?,?,?,?,?,?,?)"
        let inserts = [config.tables.customertranscation,
            'customer_account_transactions_id',
            'customer_accounts_id',
            'amount_debit',
            'amount_credit',
            'account_types_id',
            'status',
            'type',
            'tidecurrentaccountsortcode',
            'tidecurrentaccount',
            'reference',
            'transferaccount',
            'create_date',
        data.bank_transaction_id,
        data.bankaccounts_id,
        data.amount_debit,
        data.amount_credit,
            acc_typeid,
        data.status,
        data.transfertype,
        data.tidecurrentaccountsortcode,
        data.tidecurrentaccount,
        data.reference,
        data.from_to_account_no,
        data.transactiontime];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })


    }
    /**morgage*/
    MorgFind_m(bankcustomers_id) {
        let query = "SELECT * FROM ?? WHERE ??=?"
        let inserts = [config.tables.morg, 'bankcustomers_id', bankcustomers_id];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }
    Morg_m(data) {
        let query = "INSERT INTO ?? (??,??,??,??,??,??,??,??,??,??,??,??)"
        query += "  VALUES(?,?,?,?,?,?,?,?,?,?,?,?)"
        let inserts = [config.tables.morg,
            'customers_id',
            'bankcustomers_id',
            'accountholdername',
            'accountno',
            'balance',
            'interestrate',
            'emi',
            'productname',
            'renewaldate',
            'create_date',
            'tidecurrentaccount',
            'acc_current_term',
        data.bankaccounts_id,
        data.bankcustomers_id,
        data.accountholdername,
        data.accountno,
        data.account_type,
        data.tidecurrentaccount,
        data.account_creation_date,
        data.balance,
        data.term,
        data.interestrate,
        data.emi,
        data.renewaldate];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }
    MorgTransFind_m(bank_transaction_id) {
        let query = "SELECT * FROM ?? WHERE ??=?"
        let inserts = [config.tables.morgtran, 'mortgage_account_transactions_id', bank_transaction_id];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }
    MorgTrans_m(acc_typeid, data) {
        let query = "INSERT INTO ?? (??,??,??,??,??,??,??,??,??,??,??,??)"
        query += "  VALUES(?,?,?,?,?,?,?,?,?,?,?,?)"
        let inserts = [config.tables.morgtran,
            'mortgage_account_transactions_id',
            'mortgage_accounts_id',
            'amount_debit',
            'amount_credit',
            'acc_typeid',
            'description',
            'tidecurrentaccountsortcode',
            'tidecurrentaccount',
            'from_to_account_no',
            'reference',
            'create_date',
            'isactive',
        data.bank_transaction_id,
        data.bankaccounts_id,
        data.amount_debit,
        data.amount_credit,
            acc_typeid,
        data.transfertype,
        data.tidecurrentaccountsortcode,
        data.tidecurrentaccount,
        data.from_to_account_no,
        data.reference,
        data.transactiontime,
        data.status];
        query = mysql.format(query, inserts);
        return new Promise((resolve, reject) => {

            Query2(query).then((result) => {
                resolve(result);

            }).catch((error) => {
                console.log(error)
                reject(error);
            })
        })

    }
}
/** To execut it using cmd uncomment below 3 line */
// const cust=new customer();
// const init=cust.ExecuteAll();
// module.exports=init;
/** CMD TO execut node -e "require('./controller/customer_c.js').t()" */


module.exports = customer;