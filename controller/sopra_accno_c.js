// /const OauthToken = require('../services/oauth-rpa/oauthRPA');
class Sopra_AccNo_C {
    constructor() {
        const sopra_acc_m = require("../models/sopra_accno_m");
        this.accno = new sopra_acc_m();
    }
    GetAccNo(req, res) {
        this.accno.GetAccNO(0).then((result) => {
            res.status(200).json({
                status: 200,
                message: "lasted avaialbe ID",
                data: result
            }).end();
        }).catch((err) => {
            res.status(400).json({
                status: "error",
                error: "NOT FOUND",
                error_description: "No Account Number Is Available "
            }).end();
        })
    }
    async CreateAccNo(req, res) {
        let number = /^[0-9]{1,11}$/;
        let end_number = /^[0-9]{1,6}$/;

        if (req.body.start === undefined || number.test(parseInt(req.body.start)) === false && req.body.end === undefined) {
            return res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "Account To Not Provided"
            }).end();
        } else if (number.test(parseInt(req.body.start)) === true && end_number.test(parseInt(req.body.end)) === false) {
            let accno = parseInt(req.body.start);
            let result = await this.accno.CreateAccNo(accno, 1);
            return res.status(200).json({
                status: 200,
                message: "note :- end sequence may consider because is greater than 100000",
                data: result
            }).end();
        } else if (number.test(parseInt(req.body.start)) === true && end_number.test(parseInt(req.body.end)) === true) {
            let total = parseInt(req.body.end);
            if (total > 100000) {
                return res.status(400).json({
                    status: "error",
                    error: "end sequence number should not be greater than 100000",
                    error_description: " end sequence is greater than 100000"
                }).end();
            }

            try {
                await this.accno.CreateAccNo(parseInt(req.body.start), parseInt(req.body.end));
                return res.status(200).json({
                    status: 200,
                    message: "successfull",
                    data: "successfully added account no."
                }).end();
            } catch (error) {
                return res.status(400).json({
                    status: "error",
                    error: "invalid_request multiple",
                    error_description: error
                }).end();
            }

        }
        else {
            return res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "something went wrong"
            }).end();
        }
    }
    async CreateBatchAccNo(req, res) {
        let number = /^[0-9]{1,11}$/;
        let end_number = /^[0-9]{1,6}$/;

        if (req.body.start === undefined || number.test(parseInt(req.body.start)) === false && req.body.end === undefined) {
            return res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "Account To Not Provided"
            }).end();
        } else if (number.test(parseInt(req.body.start)) === true && end_number.test(parseInt(req.body.end)) === false) {
            let accno = parseInt(req.body.start);
            let result = await this.accno.CreateBatchAccNo(accno, 1);
            return res.status(200).json({
                status: 200,
                message: "note :- end sequence may consider because is greater than 100000",
                data: result
            }).end();
        } else if (number.test(parseInt(req.body.start)) === true && end_number.test(parseInt(req.body.end)) === true) {
            let total = parseInt(req.body.end);
            if (total > 100000) {
                return res.status(400).json({
                    status: "error",
                    error: "end sequence number should not be greater than 100000",
                    error_description: " end sequence is greater than 100000"
                }).end();
            }

            try {
                await this.accno.CreateBatchAccNo(parseInt(req.body.start), parseInt(req.body.end));
                return res.status(200).json({
                    status: 200,
                    message: "successfull",
                    data: "successfully added account no."
                }).end();
            } catch (error) {
                return res.status(400).json({
                    status: "error",
                    error: "invalid_request multiple",
                    error_description: error
                }).end();
            }

        }
        else {
            return res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "something went wrong"
            }).end();
        }
    }
    getAllaccno(req, res) {
        let dataBody = {}
        let tableObj = (req.query.obj) ? JSON.parse(req.query.obj) : null;
        if (tableObj && tableObj.pageDetail && tableObj.pageDetail.page) {
            tableObj.pageDetail.page = tableObj.pageDetail.page - 1;
            if (tableObj.pageDetail.page < 0) tableObj.pageDetail.page = 0;
            if (!tableObj.pageDetail.pageSize) {
                tableObj.pageDetail.pageSize = 0;
            }
            if (tableObj.sortDetail && (!tableObj.sortDetail.field || !tableObj.sortDetail.type)) {
                delete tableObj.sortDetail;
            }
            dataBody.tableobj = tableObj;

        }
        this.accno.getAllaccno(dataBody).then((data) => {
            res.status(200).json({
                status: "OK",
                message: "success",
                data: data
            }).end();
        }).catch((err) => {
            console.log(err, "------------------>err")
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: err
            }).end();
        })
    }
    getAllBatchno(req, res) {
        let dataBody = {}
        let tableObj = (req.query.obj) ? JSON.parse(req.query.obj) : null;
        if (tableObj && tableObj.pageDetail && tableObj.pageDetail.page) {
            tableObj.pageDetail.page = tableObj.pageDetail.page - 1;
            if (tableObj.pageDetail.page < 0) tableObj.pageDetail.page = 0;
            if (!tableObj.pageDetail.pageSize) {
                tableObj.pageDetail.pageSize = 0;
            }
            if (tableObj.sortDetail && (!tableObj.sortDetail.field || !tableObj.sortDetail.type)) {
                delete tableObj.sortDetail;
            }
            dataBody.tableobj = tableObj;

        }
        this.accno.getAllBatchno(dataBody).then((data) => {
            res.status(200).json({
                status: "OK",
                message: "success",
                data: data
            }).end();
        }).catch((err) => {
            console.log(err, "------------------>err")
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: err
            }).end();
        })

    }
}
module.exports = Sopra_AccNo_C;