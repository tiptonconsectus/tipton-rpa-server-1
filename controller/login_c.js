class UserLogin {
    constructor(loginhandler=null) {
        const config = require("../config/config");
        const UserModel = require("../models/user_m");
        this.bcrypt = require('bcryptjs');
       this.loginhandler=loginhandler

        this.utils = require("../services/utils/utils");
        this.userModel = new UserModel();
        this.config = config;
        this.SALT_WORK_FACTOR = 10;
       
    }
    Login(req, res) {
       
        this.loginhandler.login(req,res,function(err,result){
           if(err){
                res.render('login/login');
           }else{
                res.cookie('authorization',"Bearer "+result.access_token, {maxAge:7200000}).redirect('/admin/dashboard');
           }
           
        })

    }
    Createlogin(cb) {

    }
    hasPassword(plain, password, fn) {
        fn = fn || this.utils.createPromiseCallback();
        if (password && plain) {
            //  bcrypt.compare(plain, password, function(err, isMatch) {
            // if (err) return fn(err);
            fn(null, "isMatch");
            //  });
        } else {
            fn(null, false);
        }
        return fn.promise;

    }
    logout(req,res) {
       // console.log(req.token)
        this.loginhandler.userLogout(req,res,function(err,req){
          

            if(err){
                res.clearCookie("authorization").redirect('/');
            }else{
                res.clearCookie("authorization").redirect('/');
            }
        })

    }



    hashPassword(plain) {
        this.validatePassword(plain);

        var salt = bcrypt.genSaltSync((this.config.bcrypt !== null) ? this.config.bcrypt : SALT_WORK_FACTOR);
        return bcrypt.hashSync(plain, salt);
    };

}
module.exports = UserLogin;