// /const OauthToken = require('../services/oauth-rpa/oauthRPA');
const debug = require("debug")("server upload")
const debugerror = require("debug")("server upload error:")

class UploadFile{
    constructor() {
        const httprequest = require('../services/oauth-rpa/httprequest');
        this.http = new httprequest();
        const rpamodel = require("../models/rpaagent_m");
        this.Rpa_m = new rpamodel();

    }
     AgentUploadActionPath(req, result) {
        return new Promise((resolve, reject) => {

            let oauth = req.app.getOauthToken(result.host, result.port);
            oauth.getAccessToken(false).then(() => {
 
                this.http.HttpPostForm(req, result, oauth.token).then((success) => {

                    //   console.log()
                    let data = {
                        update_id: result.id,
                        status: 1
                    }


                    this.Rpa_m.updateAgent(data, true);

                    resolve(success);
                }).catch((err) => {
                    debugerror(err," line 32")
               
                        reject(err);
                   
                })
            }).catch((err) => {
               reject(err)
            })

        })

    }
}
module.exports=UploadFile;