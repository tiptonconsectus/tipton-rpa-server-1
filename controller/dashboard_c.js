class DashBoard {
    constructor() {
        const Dashboard_m = require('../models/dashboard_m');
        this.dashboard_m = new Dashboard_m();
        const rpamodel = require("../models/rpaagent_m");
        this.Rpa_m = new rpamodel();
    }
    getDashboard(req, res) {
        res.render('dashboard/dashboard');
    }
    getAverage(req, res) {
        this.dashboard_m.Average().then((result) => {
            res.status(200).json({
                status: "OK",
                message: "success",
                data: result
            }).end();
        }).catch((error) => {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: error
            }).end();
        });
    }
    getQueueStatus(req, res) {

    }
    // getAllAgent(){

    // }
    async getAllAgent() {
        let agentname = [];
        let data = []
        let agent = this.Rpa_m.find();
        if (agent.lenght > 0) {
            for (let i = 0; i < agent.lenght; i++) {

            }

        } else {
            throw "no agent found"
        }
    }
    getTotalAgent(req, res) {
        this.dashboard_m.PathQueues().then((success) => {
            res.status(200).json({
                status: "OK",
                message: "success",
                data: success
            }).end();
        }).catch((error) => {
           // console.log(error)
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: error
            }).end();
        })
    }
    getAgentCompleted(req,res){
        let dataBody = {}
        let tableObj = (req.query.obj) ? JSON.parse(req.query.obj) : null;
        if (tableObj && tableObj.pageDetail && tableObj.pageDetail.page) {
            tableObj.pageDetail.page = tableObj.pageDetail.page - 1;
            if (tableObj.pageDetail.page < 0) tableObj.pageDetail.page = 0;
            if (!tableObj.pageDetail.pageSize) {
                tableObj.pageDetail.pageSize = 0;
            }
            if (tableObj.sortDetail && (!tableObj.sortDetail.field || !tableObj.sortDetail.type)) {
                delete tableObj.sortDetail;
            }
            dataBody.tableobj = tableObj;

        }
        this.dashboard_m.getComplete(dataBody).then((data)=>{
            res.status(200).json({
                status: "OK",
                message: "success",
                data: data
            }).end();
        }).catch((err)=>{
            console.log(err,"------------------>err")
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: err
            }).end();
        })
    }
    getAgentPathQueues(req, res) {

        let status = null;
        if (req.query.status !== undefined) {
            if (req.query.status !== 'all' && req.query.status !== 'pending' && req.query.status !== "completed") {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    error_description: "status needs to be 'all' or 'pending' or 'completed'"
                }).end();
            } else {
                status = req.query.status;
            }
        }
        this.dashboard_m.getAgentPathQueues(status).then((success) => {
            res.status(200).json({
                status: "OK",
                message: "success",
                data: success
            }).end();
        }).catch((error) => {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "wrong type - use 'true' or 'false'"
            }).end();
        })
    }
}
module.exports = DashBoard;